function customLoading(action){

  if($("#customLoading").length == 0) {

    html =   '<div id="customLoading" class="customLoading d-none">';
    html +=   '<div class="customLoadingContainer">';
    // html +=   '<img src="'+ baseURL +'assets/genesa/img/loading.gif" alt="loading...">';
    html +=   '<div class="loader vertical-align-middle loader-circle"></div>';
    html +=   '</div></div>';
    $("body").append(html);
  }

  if(action == "show"){
    $("#customLoading").removeClass('d-none');
    setTimeout(function() {
      $("#customLoading").addClass('d-none');
    }, 120000);
  }
  else{
    $("#customLoading").addClass('d-none');
  }

}

function customNotif(title, message, type, time=3500){

  element = $('#customNotif');

  element.data('message', "<strong>"+ title +"!</strong> &nbsp;&nbsp;<br>" + message);
  element.data('icon-class', "toast-just-text toast-" + type);
  element.data('time-out', time);

  element.trigger('click');

}

function formatNumber(nStr){
  nStr += '';
  x = nStr.split('.');
  x1 = x[0];
  x2 = x.length > 1 ? '.' + x[1] : '';
  var rgx = /(\d+)(\d{3})/;
  while (rgx.test(x1)) {
  x1 = x1.replace(rgx, '$1' + '.' + '$2');
  }
  return x1 + x2;
}

function generateRandomKey(length) {
  let total            = length-1;
  let result           = '';
  let characters       = 'abcdefghijklmnopqrstuvwxyz0123456789';
  let charactersLength = characters.length;
  for (i = 0; i < total; i++ ) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return "k" + result;
}

function customNotifFUllWidth(title, message, type, time=2500){
	
	if($("#exampleTopFullWidth").length == 0) {
		html =   '<a class="btn btn-primary btn-block" id="exampleTopFullWidth" data-plugin="toastr"';
		html +=   'data-container-id="toast-topFullWidth"';
		html +=   'data-position-class="toast-top-full-width"';
		html +=   'data-show-method="slideDown"';
		html +=   'data-close-button="true"';
		html +=   'href="javascript:void(0)" role="role">Top Full Width</a>';
		$("body").append(html);
	}

  element = $('#exampleTopFullWidth');

  element.data('message', "<strong>" + title + "</strong> " + message)
  element.data('icon-class', "toast-" + type);
  element.data('time-out', time);

  element.trigger('click');

}

function deleteData(link, data){

  $('#button-delete').on( 'click', function () {

      $.ajax({
        url       : BASEURL + link,
        type      : 'post',
        beforeSend  : function(){
                        customLoading('show');
                      },
        data:data,
        dataType : 'json',
        success : function(result){
          id_delete = 0;
          $("#modal-delete").modal('hide');
          customLoading('hide');

          if (result.status == true) {
            table.ajax.reload(null, false);
            customNotif('Success', result.messages, 'success');
          } else {
            customNotif('Failed', result.messages, 'error');
          }
        }
      });
  });
}

function openLink(link){
    win = window.open(link, '_blank');
    win.focus();
}


function data_table(ajaxData, json, table="table_data"){
  Pace.track(function(){
    $('#'+table).DataTable({
      "serverSide"      : true,
      "processing"      : true,
      "ajax"            : ajaxData,
      "language"        : {
                            "emptyTable"  : "<span class='label label-danger'>Data Not Found!</span>",
                            "infoEmpty"   : "Data Kosong",
                            "processing"  : '<div class="loader vertical-align-middle loader-circle"></div>',
                            "search"      : "_INPUT_"
                          },
      "columns"         : json,
      "pageLength"      : 10,
      "ordering"        : false,
      "scrollY"         : 480,
      "scrollCollapse"  : true,
      "scrollX"         : true,
      "autoWidth"       : true,
      "bAutoWidth"      : true
    });
  });

  $('input[type="search"]').attr('placeholder','Search here...').addClass('form-control input-sm m-0');

}


function createCookie(name,value,days=1) {
  if (days) {
    var date = new Date();
      date.setTime(date.getTime()+(days*24*60*60*1000));
      var expires = "; expires="+date.toUTCString();
  }
  else var expires = "";
  document.cookie = name+"="+value+expires+"; path=/";
}

function readCookie(name) {
  var nameEQ = name + "=";
  var ca = document.cookie.split(';');
  for(var i=0;i < ca.length;i++) {
      var c = ca[i];
      while (c.charAt(0)==' ') c = c.substring(1,c.length);
      if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
  }
  return null;
}

function eraseCookie(name) {
  createCookie(name,"",-1);
}
