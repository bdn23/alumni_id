<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
	<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
	<title>Index Design</title>
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-md-6 offset-md-3 mt-3">
				<h1 class="text-center">Index Page</h1>
				<table class="table table-striped table-sm mt-3">
					<thead>
						<th class="text-center" width="30px">No</th>
						<th>Page</th>
						<th width="30px">Link</th>
					</thead>
					<?php
						$lists = [
									[
										'name' => 'Home',
										'link' => 'home'
									],
									[
										'name' => 'Biografi',
										'link' => 'biography'
									],
									[
										'name' => 'Biografi Detail',
										'link' => 'biography-detail'
									],
									[
										'name' => 'Undang Undang',
										'link' => 'constitution'
									],
									[
										'name' => 'Jurnal',
										'link' => 'journal'
									],
									[
										'name' => 'Login',
										'link' => 'login'
									],
									[
										'name' => 'Signup',
										'link' => 'signup'
									],
									[
										'name' => 'Paket Berlangganan',
										'link' => 'package'
									],
									[
										'name' => 'Profesi & Jasa Alumni',
										'link' => 'profession'
									],
									[
										'name' => 'Profesi & Jasa Alumni Detail',
										'link' => 'profession-detail'
									],
									[
										'name' => 'UMKM',
										'link' => 'umkm'
									],
									[
										'name' => 'UMKM Detail',
										'link' => 'umkm-detail'
									],
									[
										'name' => 'Berita',
										'link' => 'news'
									],
									[
										'name' => 'Berita Detail',
										'link' => 'news-detail'
									],
									[
										'name' => 'Info Detail',
										'link' => 'info-detail'
									],
								 ];
					?>
					<tbody>
						<?php
							foreach ($lists as $key => $v) {
						?>

							<tr>
								<td class="text-center"><?= $key+1 ?></td>
								<td><?= $v['name'] ?></td>
								<td><a target="_blank" href="design/<?= $v['link'] ?>.php" class="btn btn-primary btn-sm"><i class="fa fa-share-square-o"></i></a></td>
							</tr>
						<?php
							}
						?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</body>
</html>