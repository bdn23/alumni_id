<?php
    $bgColor = "bg-white";
    include "header.php";
?>

<main class="position-relative">
    <!--main content-->
    <div class="main_content sidebar_right pb-50">
        <div class="container">
        <div class="single-header-3 single-header-2 row no-gutters mb-50 background12 border-radius-5">                    
            <div class="col-lg-6 col-md-12 align-center-vertical order-2 order-lg-1">
                <div class="entry-header entry-header-1">
                <div class="entry-meta meta-0 font-small mb-30"><a href="category.html"><span class="post-cat background2 color-white">Biografi</span></a></div>
                <h1 class="post-title">
                    <a href="single.html">Dr. Ary Zulfikar</a>
                </h1>
                <div class="bt-1 border-color-1 mt-30 mb-30"></div>
                <div class="single-social-share clearfix ">
                    <p>
                        Experienced Founder with a demonstrated history of working in the law practice industry. Skilled in Company Restructuring, Merger & Acquisition, Negotiation, International Law, Risk Management, Joint Ventures, and Business. Strong business development professional with a Doctor of Law - JD focused in Investment from Universitas Padjadjaran.
                    </p>
                    <ul class="d-inline-block list-inline float-right">
                        <li class="list-inline-item"><a class="social-icon facebook-icon text-xs-center color-white" target="_blank" href="#"><i class="ti-facebook"></i></a></li>
                        <li class="list-inline-item"><a class="social-icon twitter-icon text-xs-center color-white" target="_blank" href="#"><i class="ti-twitter-alt"></i></a></li>
                        <li class="list-inline-item"><a class="social-icon instagram-icon text-xs-center color-white" target="_blank" href="#"><i class="ti-instagram"></i></a></li>
                    </ul>
                </div>
                </div>
            </div>
            <!--end entry header-->
            <div class="col-lg-6 col-md-12 order-1 order-lg-2">
                <figure class="single-thumnail img-hover-slide mb-0" style="background-image: url('assets/imgs/ava-1.jpg')"></figure>
            </div>
        </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-7 col-md-12 col-sm-12">
                    <div class="entry-main-content">
                        <h2>Perjalanan karier</h2>
                        <hr class="wp-block-separator is-style-wide">
                        <p>Pria yang semula bercita-cita menjadi wartawan seperti profesi yang dilakoni ayahnya itu menuturkan, perjalanan kariernya di dunia hukum. Selepas menuntaskan pendidikan Sarjana Hukum di Universitas Padjajaran, Bandung, ia menjadi konsultan pasar modal di firma hukum Kartini Muljadi & Rekan. Untuk memperkaya pengalamannya, terutama di area pasar modal, ia kemudian bergabung dengan lembaga penunjang pasar modal, seiring dengan berkembangnya kegiatan pasar modal di era tahun 1990-an, yaitu PT Kustodian Depositori Efek Indonesia (KDEI).</p>

                        <p>Setelah berlakunya Undang-undang No. 8 Tahun 1995 tentang Pasar Modal yang mewajibkan fungsi Kustodian dan Kliring transaksi Efek, kemudian ia bergabung dengan PT Kustodian Sentral Efek Indonesia (KSEI), sehingga ikut terlibat dalam penyusunan peraturan transaksi efek tanpa warkat (scripless) pada awal dimulainya transaksi scripless di pasar modal Indonesia.</p>

                        <p>>Pada saat krisis keuangan melanda Asia Tenggara tahun 1998 dan Indonesia termasuk negara yang terkena imbas krisis keuangan, ia pun bergabung membantu Pemerintah Republik Indonesia pada lembaga penyehatan perbankan, yaitu Badan Penyehatan Perbankan Nasional (BPPN). Dan juga turut andil sebagai anggota tim perumus yang melakukan studi banding ke beberapa negara untuk merumuskan tentang pembentukan Lembaga Penjamin Simpanan (LPS). Dalam Badan ini ia telah menghadapi berbagai jenis masalah hukum sehubungan dengan rekapitalisasi bank dan resolusi aset.</p>

                        <h2>Kehidupan pribadi</h2>
                        <hr class="wp-block-separator is-style-wide">
                        <p>Ary Zulfikar dikenal sebagai sosok yang hangat dan simpel, saat ditemui Men’s Obsession di kantornya yang berada di bilangan Cilandak, Jakarta Selatan. Dengan santai ia mengungkapkan makna pencapaian meraih gelar prestise sebagai salah satu pengacara top di Indonesia.</p>

                        <p>“Pada prinsipnya dalam mengerjakan sesuatu, kita jangan berpatokan pada apa yang akan kita dapatkan nanti. Namun berikan yang terbaik, dengan sendirinya orang akan memberikan apresiasi kepada kita,” tuturnya seraya tersenyum.</p>

                        <p>Banyak suka dan duka yang dialami Zulfikar sepanjang perjalanan kariernya di bidang hukum, terlebih pada saat melakukan penanganan penyelesaian perusahaaan-perusahaan yang terkena imbas dari krisis keuangan yang multidimensi pada saat itu. Tuntutan pekerjaan untuk menyelesaikan suatu tugas yang merupakan tanggung jawabnya, membuat keseharian Zulfikar banyak dihabiskan di ruang kerja saat itu, Bahkan kadang mengorbankan waktunya untuk kegiatan lain.</p>

                        <p>Berbekal pengalaman-pengalaman yang telah ia dapatkan, pada 2004, pria yang akrab disapa ‘Azoo’ oleh teman dan koleganya itu, membuka firma hukum sendiri bernama AZP Legal Consultants.</p>
                    </div>
                </div>
                <!--col-lg-8-->
                <!--Right sidebar-->
                <div class="col-lg-5 col-md-12 col-sm-12 primary-sidebar sticky-sidebar">
                    <div class="widget-area pl-30">
                        <!--Widget about-->
                        <div class="sidebar-widget widget-about mb-50 pt-30 pr-30 pb-30 pl-30 background12 border-radius-5">
                            <h3 class="mb-40 text-info">Biofile<img class="about-author-img float-right ml-30" src="assets/imgs/ava-1.jpg" alt=""></h3>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="widget-header position-relative mb-30">
                                        <h4 class="widget-title mb-30 text-uppercase color1 font-weight-ultra">Pendidikan Formal</h4>
                                        <div class="letter-background">PF</div>
                                    </div>
                                    <ul class="ml-2" style="list-style-type: square;">
                                        <li class="mb-3">
                                            <h5>Universitas Padjajaran</h5>
                                            <span>Doctor of Law - JD . Inversment . Dr.</span><br> <small class="badge badge-success">2016 - 2019</small>
                                        </li>
                                        <li class="mb-3">
                                            <h5>Universitas Gajah Mada (UGM)</h5>
                                            <span>Postgraduate . bsiness law</span><br> <small class="badge badge-success">2012 - 2014</small>
                                        </li>
                                        <li class="mb-3">
                                            <h5>Universitas Padjajaran</h5>
                                            <span>SH . Civil Law</span><br> <small class="badge badge-success">1989 - 1993</small>
                                        </li>
                                    </ul>
                                    <hr class="wp-block-separator is-style-wide">
                                </div>
                            </div>
                           <!--  <div class="row">
                                <div class="col-md-12">
                                    <div class="widget-header position-relative mb-30">
                                        <h4 class="widget-title mb-30 text-uppercase color1 font-weight-ultra">Pendidikan Informal</h4>
                                        <div class="letter-background">PI</div>
                                    </div>
                                    <ul class="ml-2" style="list-style-type: square;">
                                        <li>
                                            Kursus Model (2012 - 2016)
                                        </li>
                                        <li>
                                            Kursus Bahasa Thailand (2012 - 2016)
                                        </li>
                                    </ul>
                                    <hr class="wp-block-separator is-style-wide">
                                </div>
                            </div> -->
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="widget-header position-relative mb-30">
                                        <h4 class="widget-title mb-30 text-uppercase color1 font-weight-ultra">Pengalaman</h4>
                                        <div class="letter-background">P</div>
                                    </div>
                                    <ul class="ml-2" style="list-style-type: square;">
                                        <li class="mb-3">
                                            <h5>Executive Director</h5>
                                            <span>Indonesia Deposit Insurance Corporation (IDIC)</span><br> <small class="badge badge-info">2020</small>
                                        </li>
                                        <li class="mb-3">
                                            <h5>Founder</h5>
                                            <span>PT Rising Star Property</span><br> <small class="badge badge-info">2018</small>
                                        </li>
                                        <li class="mb-3">
                                            <h5>Lecturer</h5>
                                            <span>Law Faculty, Pancasila University</span><br> <small class="badge badge-info">2017</small>
                                        </li>
                                    </ul>
                                    <hr class="wp-block-separator is-style-wide">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--End sidebar-->
            </div>
        </div>
    </div>
</main>
<?php
    include "footer.php";
?>