<?php
    include "header.php";
?>

    <div class="pt-50 pb-0 background-white">
        <div class="container mb-50">
            <div class="sidebar-widget">
                <div class="widget-header position-relative mb-30">
                    <h5 class="widget-title mb-30 text-uppercase color1 font-weight-ultra">Biografi</h5>
                    <div class="letter-background">B</div>
                </div>
                <div class="post-carausel-2 post-module-1 row">
                    <div class="col">
                        <div class="post-thumb position-relative">
                            <div class="thumb-overlay img-hover-slide border-radius-5 position-relative" style="background-image: url('assets/imgs/ava-1.jpg')">
                                <a class="img-link" href="single.html"></a>
                                <div class="post-content-overlay">
                                    <h6 class="post-title">
                                        <a class="color-white" href="single.html">Dr. Ary Zulfikar</a>
                                    </h6>
                                    <div class="entry-meta meta-1 font-small color-grey mt-10 pr-5 pl-5">
                                        <span>Executive Director at Indonesia Deposit Insurance Corporation (IDIC)/Lembaga Penjamin Simpanan (LPS)</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="post-thumb position-relative">
                            <div class="thumb-overlay img-hover-slide border-radius-5 position-relative" style="background-image: url('assets/imgs/ava-2.jpg')">
                                <a class="img-link" href="single.html"></a>
                                <div class="post-content-overlay">
                                    <h6 class="post-title">
                                        <a class="color-white" href="single.html">Prof. Dr. Hj. Rina Indiastuti</a>
                                    </h6>
                                    <div class="entry-meta meta-1 font-small color-grey mt-10 pr-5 pl-5">
                                        <span>Rektor Universitas Padjadjaran</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="post-thumb position-relative">
                            <div class="thumb-overlay img-hover-slide border-radius-5 position-relative" style="background-image: url('assets/imgs/ava-3.jpg')">
                                <a class="img-link" href="single.html"></a>
                                <div class="post-content-overlay">
                                    <h6 class="post-title">
                                        <a class="color-white" href="single.html">Hotman Paris</a>
                                    </h6>
                                    <div class="entry-meta meta-1 font-small color-grey mt-10 pr-5 pl-5">
                                        <span>Pengacara</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="post-thumb position-relative">
                            <div class="thumb-overlay img-hover-slide border-radius-5 position-relative" style="background-image: url('assets/imgs/ava-4.jpg')">
                                <a class="img-link" href="single.html"></a>
                                <div class="post-content-overlay">
                                    <h6 class="post-title">
                                        <a class="color-white" href="single.html">Melody Nurramdhani</a>
                                    </h6>
                                    <div class="entry-meta meta-1 font-small color-grey mt-10 pr-5 pl-5">
                                        <span>Penyanyi</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="post-thumb position-relative">
                            <div class="thumb-overlay img-hover-slide border-radius-5 position-relative" style="background-image: url('assets/imgs/ava-5.jpg')">
                                <a class="img-link" href="single.html"></a>
                                <div class="post-content-overlay">
                                    <h6 class="post-title">
                                        <a class="color-white" href="single.html">Ernest Prakasa</a>
                                    </h6>
                                    <div class="entry-meta meta-1 font-small color-grey mt-10 pr-5 pl-5">
                                        <span>Sutradara</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="post-thumb position-relative">
                            <div class="thumb-overlay img-hover-slide border-radius-5 position-relative" style="background-image: url('assets/imgs/ava-6.jpg')">
                                <a class="img-link" href="single.html"></a>
                                <div class="post-content-overlay">
                                    <h6 class="post-title">
                                        <a class="color-white" href="single.html">Chika Jessica</a>
                                    </h6>
                                    <div class="entry-meta meta-1 font-small color-grey mt-10 pr-5 pl-5">
                                        <span>Artis</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="main_content sidebar_right mt-5 pb-50">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-md-12 col-sm-12">
                        <!--loop-list-->

                        <div class="widget-header position-relative mb-30">
                            <h5 class="widget-title mb-30 text-uppercase color1 font-weight-ultra">Profesi & Jasa Alumni</h5>
                            <div class="letter-background">P&J</div>
                        </div>
                        <div class="loop-grid row">
                            <article class="col-lg-6 mb-50 animate-conner">
                                <div class="post-thumb d-flex mb-30 border-radius-5 img-hover-scale animate-conner-box">
                                    <a href="single.html">
                                        <img src="assets/imgs/pnj-1.jpg" alt="">
                                    </a>
                                </div>
                                <div class="post-content">
                                    <div class="entry-meta meta-0 font-small mb-15">
                                        <a href="category.html"><span class="post-cat background2 color-white">Jasa</span></a>
                                    </div>
                                    <h3 class="post-title">
                                        <a href="single.html">Klinik Gigi</a>
                                    </h3>
                                    <div class="entry-meta meta-1 font-small color-grey mt-15 mb-15">
                                        <span class="post-by">By <a href="author.html">Melody Nurramdhani</a></span>
                                    </div>
                                    <div class="post-excerpt mb-25">
                                        <p>
                                            <?= lorem(16) ?>
                                        </p>
                                        <small>
                                            <table class="table table-sm borderless">
                                                <tr>
                                                    <td><i class="fas fa-map-marker-alt"></i></td>
                                                    <td>Jalan Panjang Lebar RT 01 / RW 02 <br> Ciawi, Bogor</td>
                                                </tr>
                                                <tr>
                                                    <td><i class="fas fa-phone"></i></td>
                                                    <td>0857 11200 377</td>
                                                </tr>
                                            </table>
                                        </small>
                                    </div>
                                </div>
                            </article>
                            <article class="col-lg-6 mb-50 animate-conner">
                                <div class="post-thumb d-flex mb-30 border-radius-5 img-hover-scale animate-conner-box">
                                    <a href="single.html">
                                        <img src="assets/imgs/pnj-2.jpg" alt="">
                                    </a>
                                </div>
                                <div class="post-content">
                                    <div class="entry-meta meta-0 font-small mb-15">
                                        <a href="category.html"><span class="post-cat background3 color-white">Produk</span></a>
                                    </div>
                                    <h3 class="post-title">
                                        <a href="single.html">Konsultan Bisnis</a>
                                    </h3>
                                    <div class="entry-meta meta-1 font-small color-grey mt-15 mb-15">
                                        <span class="post-by">By <a href="author.html">Syafira Razak</a></span>
                                    </div>
                                    <div class="post-excerpt mb-25">
                                        <p>
                                            <?= lorem(16) ?>
                                        </p>
                                        <small>
                                            <table class="table table-sm borderless">
                                                <tr>
                                                    <td><i class="fas fa-map-marker-alt"></i></td>
                                                    <td>Jalan Panjang Lebar RT 01 / RW 02 <br> Ciawi, Bogor</td>
                                                </tr>
                                                <tr>
                                                    <td><i class="fas fa-phone"></i></td>
                                                    <td>0857 11200 377</td>
                                                </tr>
                                            </table>
                                        </small>
                                    </div>
                                </div>
                            </article>
                            <article class="col-lg-6 mb-50 animate-conner">
                                <div class="post-thumb d-flex mb-30 border-radius-5 img-hover-scale animate-conner-box">
                                    <a href="single.html">
                                        <img src="assets/imgs/pnj-3.jpg" alt="">
                                    </a>
                                </div>
                                <div class="post-content">
                                    <div class="entry-meta meta-0 font-small mb-15">
                                        <a href="category.html"><span class="post-cat background2 color-white">Jasa</span></a>
                                    </div>
                                    <h3 class="post-title">
                                        <a href="single.html">Jasa Pindah Rumah</a>
                                    </h3>
                                    <div class="entry-meta meta-1 font-small color-grey mt-15 mb-15">
                                        <span class="post-by">By <a href="author.html">Ernest Prakasa</a></span>
                                    </div>
                                    <div class="post-excerpt mb-25">
                                        <p>
                                            <?= lorem(16) ?>
                                        </p>
                                        <small>
                                            <table class="table table-sm borderless">
                                                <tr>
                                                    <td><i class="fas fa-map-marker-alt"></i></td>
                                                    <td>Jalan Panjang Lebar RT 01 / RW 02 <br> Ciawi, Bogor</td>
                                                </tr>
                                                <tr>
                                                    <td><i class="fas fa-phone"></i></td>
                                                    <td>0857 11200 377</td>
                                                </tr>
                                            </table>
                                        </small>
                                    </div>
                                </div>
                            </article>
                            <article class="col-lg-6 mb-50 animate-conner">
                                <div class="post-thumb d-flex mb-30 border-radius-5 img-hover-scale animate-conner-box">
                                    <a href="single.html">
                                        <img src="assets/imgs/pnj-4.jpg" alt="">
                                    </a>
                                </div>
                                <div class="post-content">
                                    <div class="entry-meta meta-0 font-small mb-15">
                                        <a href="category.html"><span class="post-cat background2 color-white">Jasa</span></a>
                                    </div>
                                    <h3 class="post-title">
                                        <a href="single.html">Software House</a>
                                    </h3>
                                    <div class="entry-meta meta-1 font-small color-grey mt-15 mb-15">
                                        <span class="post-by">By <a href="author.html">Soleh Solehun</a></span>
                                    </div>
                                    <div class="post-excerpt mb-25">
                                        <p>
                                            <?= lorem(16) ?>
                                        </p>
                                        <small>
                                            <table class="table table-sm borderless">
                                                <tr>
                                                    <td><i class="fas fa-map-marker-alt"></i></td>
                                                    <td>Jalan Panjang Lebar RT 01 / RW 02 <br> Ciawi, Bogor</td>
                                                </tr>
                                                <tr>
                                                    <td><i class="fas fa-phone"></i></td>
                                                    <td>0857 11200 377</td>
                                                </tr>
                                            </table>
                                        </small>
                                    </div>
                                </div>
                            </article>
                        </div>
                        <!--pagination-->
                        <div class="pagination-area pt-30 text-center bt-1 border-color-1">
                            <div class="container">
                                <div class="row">
                                    <div class="col-xl-12 atext-center">
                                        <a href="" class="btn btn-dark text-center">Lihat Selengkapnya</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12 col-sm-12 primary-sidebar sticky-sidebar">

                        <div class="widget-header position-relative mb-30">
                            <h5 class="widget-title text-primary mb-30 text-uppercase color1 font-weight-ultra">Undang-undang Terbaru</h5>
                            <div class="letter-background">UU</div>
                        </div>
                        <div class="widget-area pl-30">
                            <!--Widget latest posts style 1-->
                            <div class="sidebar-widget widget_alitheme_lastpost mb-50">
                                <div class="widget-header position-relative mb-20 pb-10">
                                    <h5 class="widget-title mb-10">Undang-Undang Nomor 32 Tahun 2002 tentang Penyiaran</h5>
                                    <div class="bt-1 border-color-1"></div>
                                </div>
                                <div class="post-block-list post-module-1">
                                    <p><?= lorem(20) ?></p>
                                    <a href="single.html" class="read-more">Baca Selengkapnya</a>
                                </div>
                            </div>
                            <div class="sidebar-widget widget_alitheme_lastpost mb-50">
                                <div class="widget-header position-relative mb-20 pb-10">
                                    <h5 class="widget-title mb-10">RUU tentang Pelindungan Data Pribadi</h5>
                                    <div class="bt-1 border-color-1"></div>
                                </div>
                                <div class="post-block-list post-module-1">
                                    <p><?= lorem(20) ?></p>
                                    <a href="single.html" class="read-more">Baca Selengkapnya</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--  Recent Articles start -->
    <div class="recent-area pt-50 pb-50">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-12">
                    <div class="widget-header position-relative mb-30">
                        <h5 class="widget-title mb-30 text-uppercase color1 font-weight-ultra">ARTIKEL TERBARU</h5>
                        <div class="letter-background">Artikel</div>
                    </div>
                    <div class="loop-list">
                        <article class="row mb-50">
                            <div class="col-md-6">
                                <div class="post-thumb position-relative thumb-overlay mr-20">
                                    <div class="img-hover-slide border-radius-5 position-relative" style="background-image: url('assets/imgs/news-2.jpg')">
                                        <a class="img-link" href="single.html"></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 align-center-vertical">
                                <div class="post-content">
                                    <div class="entry-meta meta-0 font-small mb-15"><a href="category.html"><span class="post-cat background2 color-white">Info</span></a></div>
                                    <h4 class="post-title">
                                        <a href="single.html">Inilah Visi Misi Dr. Ary Zulfikar Calon Ketua IKA Universitas Padjadjaran</a>
                                    </h4>
                                    <div class="entry-meta meta-1 font-small color-grey mt-15 mb-15">
                                        <span class="post-on"><i class="ti-marker-alt"></i>25 April 2020</span>
                                        <span class="time-reading"><i class="ti-timer"></i>10 mins read</span>
                                        <span class="hit-count"><i class="ti-bolt"></i> 159k Views</span>
                                    </div>
                                    <p class="font-medium">These people envy me for having a lifestyle they don’t have, but the truth is, sometimes I envy their lifestyle instead. Struggling to sell one multi-million dollar home currently.</p>
                                    <a class="readmore-btn font-small text-uppercase font-weight-ultra" href="single.html">Baca Selengkapnya<i class="ti-arrow-right ml-5"></i></a>
                                </div>
                            </div>
                        </article>
                        <article class="row mb-50">
                            <div class="col-md-6">
                                <div class="post-thumb position-relative thumb-overlay mr-20">
                                    <div class="img-hover-slide border-radius-5 position-relative" style="background-image: url('assets/imgs/news-3.jpg')">
                                        <a class="img-link" href="single.html"></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 align-center-vertical">
                                <div class="post-content">
                                    <div class="entry-meta meta-0 font-small mb-15"><a href="category.html"><span class="post-cat background3 color-white">Technology</span></a></div>
                                    <h4 class="post-title">
                                        <a href="single.html">Mumpung Jualan Surat Bebas Corona Palsu di Tengah Pandemi</a>
                                    </h4>
                                    <div class="entry-meta meta-1 font-small color-grey mt-15 mb-15">
                                        <span class="post-on"><i class="ti-marker-alt"></i>25 April 2020</span>
                                        <span class="time-reading"><i class="ti-timer"></i>10 mins read</span>
                                        <span class="hit-count"><i class="ti-bolt"></i> 159k Views</span>
                                    </div>
                                    <p class="font-medium">We live in a world where disruption and dynamism reign supreme and businesses must be ready to adapt to the many unpredictable changes that come with this.</p>
                                    <a class="readmore-btn font-small text-uppercase font-weight-ultra" href="single.html">Baca Selengkapnya<i class="ti-arrow-right ml-5"></i></a>
                                </div>
                            </div>
                        </article>
                        <article class="row mb-50">
                            <div class="col-md-6">
                                <div class="post-thumb position-relative thumb-overlay mr-20">
                                    <div class="img-hover-slide border-radius-5 position-relative" style="background-image: url('assets/imgs/news-4.jpg')">
                                        <a class="img-link" href="single.html"></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 align-center-vertical">
                                <div class="post-content">
                                    <div class="entry-meta meta-0 font-small mb-15"><a href="category.html"><span class="post-cat background1 color-white">Sport</span></a></div>
                                    <h4 class="post-title">
                                        <a href="single.html">Bantuan Kasur Lipat untuk Ruang Perawatan Tambahan</a>
                                    </h4>
                                    <div class="entry-meta meta-1 font-small color-grey mt-15 mb-15">
                                        <span class="post-on"><i class="ti-marker-alt"></i>25 April 2020</span>
                                        <span class="time-reading"><i class="ti-timer"></i>10 mins read</span>
                                        <span class="hit-count"><i class="ti-bolt"></i> 159k Views</span>
                                    </div>
                                    <p class="font-medium">At the Emmys, broadcast scripted shows created by people of color gained ground relative to those pitched by White show creators, while broadcast scripted shows.</p>
                                    <a class="readmore-btn font-small text-uppercase font-weight-ultra" href="single.html">Baca Selengkapnya<i class="ti-arrow-right ml-5"></i></a>
                                </div>
                            </div>
                        </article>
                    </div>
                    <!--Start pagination -->
                    <div class="pagination-area pt-30 text-center bt-1 border-color-1">
                        <div class="container">
                            <div class="row">
                                <div class="col-xl-12 atext-center">
                                    <a href="" class="btn btn-dark text-center">Lihat Selengkapnya</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End pagination  -->
                </div>
                <div class="col-lg-4 col-md-12">
                    <div class="widget-area">
                        <!--taber-->
                        <div class="sidebar-widget widget-taber mb-30">
                            <div class="widget-taber-content background-white pt-30 pb-30 pl-30 pr-30 border-radius-5">
                                <nav class="tab-nav float-none mb-20">
                                    <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                        <a class="nav-item nav-link active" id="nav-popular-tab" data-toggle="tab" href="#nav-popular" role="tab" aria-controls="nav-popular" aria-selected="true">Berita Terbaru</a>
                                    </div>
                                </nav>
                                <div class="tab-content">
                                    <!--Tab Popular-->
                                    <div class="tab-pane fade show active" id="nav-popular" role="tabpanel" aria-labelledby="nav-popular-tab">
                                        <div class="row">
                                            <div class="col-md-12 mb-20">  
                                                <div class="post-thumb position-relative thumb-overlay mb-15">
                                                    <div class="img-hover-slide border-radius-5 position-relative" style="background-image: url('assets/imgs/news-1.jpg')">
                                                        <a class="img-link" href="single.html"></a>
                                                    </div>
                                                </div>
                                                <div class="post-content">
                                                    <h4 class="post-title">
                                                        <a href="single.html">Vaksin Virus Covid-19 Sudah Ditemukan</a>
                                                    </h4>
                                                    <div class="entry-meta meta-1 font-small color-grey mt-15 mb-15">
                                                        <span class="post-on"><i class="ti-marker-alt"></i>25 April 2020</span>
                                                        <span class="time-reading"><i class="ti-timer"></i>10 mins read</span>
                                                        <span class="hit-count"><i class="ti-bolt"></i> 159k Views</span>
                                                    </div>
                                                    <p class="font-medium">These people envy me for having a lifestyle they don’t have, but the truth is, sometimes I envy their lifestyle instead. Struggling to sell one multi-million dollar home currently.</p>
                                                    <a class="readmore-btn font-small text-uppercase font-weight-ultra" href="single.html">Baca Selengkapnya<i class="ti-arrow-right ml-5"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="post-block-list post-module-1">
                                            <ul class="list-post">
                                                <li class="mb-30">
                                                    <div class="d-flex">
                                                        <div class="post-thumb d-flex mr-15 border-radius-5 img-hover-scale">
                                                            <a href="single.html">
                                                                <img src="assets/imgs/news-2.jpg" alt="">
                                                            </a>
                                                        </div>
                                                        <div class="post-content media-body">
                                                            <div class="entry-meta meta-0 mb-10">
                                                                <a href="category.html"><span class="post-in background5 color-white font-small">World</span></a>
                                                            </div>
                                                            <h6 class="post-title mb-10 text-limit-2-row">Inilah Visi Misi Dr. Ary Zulfikar Calon Ketua IKA Universitas Padjadjaran</h6>
                                                            <div class="entry-meta meta-1 font-x-small color-grey">
                                                                <span class="post-on">25 April</span>
                                                                <span class="hit-count has-dot">26k Views</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="mb-30">
                                                    <div class="d-flex">
                                                        <div class="post-thumb d-flex mr-15 border-radius-5 img-hover-scale">
                                                            <a href="single.html">
                                                                <img src="assets/imgs/news-3.jpg" alt="">
                                                            </a>
                                                        </div>
                                                        <div class="post-content media-body">
                                                            <div class="entry-meta meta-0 mb-10">
                                                                <a href="category.html"><span class="post-in background7 color-white font-small">Films</span></a>
                                                            </div>
                                                            <h6 class="post-title mb-10 text-limit-2-row">Aji Mumpung Jualan Surat Bebas Corona Palsu di Tengah Pandemi</h6>
                                                            <div class="entry-meta meta-1 font-x-small color-grey mt-10">
                                                                <span class="post-on">25 April</span>
                                                                <span class="hit-count has-dot">37k Views</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="mb-30">
                                                    <div class="d-flex">
                                                        <div class="post-thumb d-flex mr-15 border-radius-5 img-hover-scale">
                                                            <a href="single.html">
                                                                <img src="assets/imgs/news-4.jpg" alt="">
                                                            </a>
                                                        </div>
                                                        <div class="post-content media-body">
                                                            <div class="entry-meta meta-0 mb-10">
                                                                <a href="category.html"><span class="post-in background2 color-white font-small">Travel</span></a>
                                                            </div>
                                                            <h6 class="post-title mb-10 text-limit-2-row">Bantuan Kasur Lipat untuk Ruang Perawatan Tambahan</h6>
                                                            <div class="entry-meta meta-1 font-x-small color-grey mt-10">
                                                                <span class="post-on">25 April</span>
                                                                <span class="hit-count has-dot">54k Views</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="d-flex">
                                                        <div class="post-thumb d-flex mr-15 border-radius-5 img-hover-scale">
                                                            <a href="single.html">
                                                                <img src="assets/imgs/news-5.jpg" alt="">
                                                            </a>
                                                        </div>
                                                        <div class="post-content media-body">
                                                            <div class="entry-meta meta-0 mb-10">
                                                                <a href="category.html"><span class="post-in background3 color-white font-small">Beauty</span></a>
                                                            </div>
                                                            <h6 class="post-title mb-10 text-limit-2-row">Bertambah 11, Positif Corona di Bengkulu Jadi 53 Kasus</h6>
                                                            <div class="entry-meta meta-1 font-x-small color-grey mt-10">
                                                                <span class="post-on">25 April</span>
                                                                <span class="hit-count has-dot">126k Views</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Ads -->
                        <div class="sidebar-widget widget-ads mb-30 text-center">
                            <a href="https://vimeo.com/333834999" class="play-video" data-animate="zoomIn" data-duration="1.5s" data-delay="0.1s">
                                <img class="d-inline-block" src="http://via.placeholder.com/432x200" alt="">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Recent Articles End -->

    <!--  Recent Articles start -->
    <div class="recent-area pt-10 pb-50">
    <!--Editor's Picked Start-->
    <div class="widgets-post-carausel-1 mb-40">
        <div class="pl-2 pr-2">
            <div class="post-carausel-1 border-radius-10 bg-white">
                <div class="row no-gutters">
                    <div class="col col-2-5 background6 editor-picked-left d-none d-lg-block">
                        <div class="editor-picked">
                            <h4>Event & Gallery</h4>
                            <p class="font-medium color-grey mt-20 mb-30">The featured articles are selected by experienced editors. It is also based on the reader's rating. These posts have a lot of interest.</p>
                            <a href="single.html" class="read-more">View More</a>
                            <div class="post-carausel-1-arrow"></div>
                        </div>
                    </div>
                    <div class="col col-3-5 col-md-12">
                        <div class="post-carausel-1-items row">

                            <?php
                                for ($i=2010; $i < 2019; $i++) { 
                            ?>
                                <div class="slider-single col">
                                    <h6 class="post-title pr-5 pl-5 mb-10 text-limit-2-row"><a href="single.html">Wisuda Tahun <?= $i ?></a></h6>
                                    <div class="img-hover-scale border-radius-5 hover-box-shadow">

                                        <a href="assets/imgs/gallery-1.jpg" class="border-radius-5 pop-image" data-animate="zoomIn" data-duration="1.5s" data-delay="0.1s"><img class="border-radius-5" src="assets/imgs/gallery-1.jpg" alt=""></a>
                                    </div>
                                    <div class="entry-meta meta-1 font-small color-grey mt-10 pr-5 pl-5">
                                        <span class="post-on float-right"><i class="ti-calendar mr-5"></i> 02 Jan <?= $i ?></span>
                                    </div>
                                </div>
                                <!--end slider single-->
                            <?php
                                }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Editor's Picked End-->
    </div>
    <!--Recent Articles End -->

<?php
    include "footer.php";
?>