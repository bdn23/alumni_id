<?php
    include "header.php";
?>

    <link rel="stylesheet" href="assets/plugin/treeview/tree_view.css">
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>

    <div class="container pt-10 pb-50">
        <div class="row mb-30">
            <div class="col-md-6 offset-md-3">
                <div class="widget-taber-content background-white pt-30 pb-30 pl-30 pr-30 border-radius-5"> 
                    
                    <div class="widget-header position-relative mb-30">
                        <h5 class="widget-title mb-30 text-uppercase color1 font-weight-ultra">Signup</h5>
                        <div class="letter-background">S</div>
                    </div>
                    <hr>
                    <form class="form-contact comment_form" action="#" id="commentForm">

                        <div class="row">                          
                            <div class="col-12">
                              <div class="form-group">
                                <label >Email</label>
                                <input type="email" class="form-control" placeholder="Isi Email">
                              </div>
                            </div>
                        </div>

                        <div class="row">                          
                            <div class="col-12">
                              <div class="form-group">
                                <label >Nama Lengkap</label>
                                <input type="text" class="form-control" placeholder="Isi Nama Lengkap">
                              </div>
                            </div>
                        </div>

                        <div class="row">                          
                            <div class="col-12">
                              <div class="form-group">
                                <label >Tanggal Lahir</label>
                                <input type="date" class="form-control" placeholder="Isi">
                              </div>
                            </div>
                        </div>


                        <div class="row">                          
                            <div class="col-12">
                              <div class="form-group">
                                <label >Tempat Lahir</label>
                                <input type="text" class="form-control" placeholder="Isi Tempat Lahir">
                              </div>
                            </div>
                        </div>

                        <div class="row">                          
                            <div class="col-12">
                              <div class="form-group">
                                <label >Janis Kelamin</label>
                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-check form-check-inline">
                                          <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
                                          <label class="form-check-label" for="inlineRadio1">Laki-laki</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                          <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2">
                                          <label class="form-check-label" for="inlineRadio2">Perempuan</label>
                                        </div>
                                    </div>
                                </div>
                              </div>
                            </div>
                        </div>

                        <div class="row">                          
                            <div class="col-12">
                              <div class="form-group">
                                <label >No Telp</label>
                                <input type="text" class="form-control" placeholder="Isi No Telp">
                              </div>
                            </div>
                        </div>
                        <hr>
                        <h3>Informasi Tempat Tinggal</h3>

                        <div class="row pb-10">                          
                            <div class="col-12">
                              <div class="form-group">
                                <label >Negara</label>
                                <select class="form-control"></select>
                              </div>
                            </div>
                        </div>
                        
                        <div class="row pb-10">                          
                            <div class="col-12">
                              <div class="form-group">
                                <label >Provinsi</label>
                                <select class="form-control"></select>
                              </div>
                            </div>
                        </div>

                        <div class="row pb-10">                          
                            <div class="col-12">
                              <div class="form-group">
                                <label >Kota</label>
                                <select class="form-control"></select>
                              </div>
                            </div>
                        </div>

                        <div class="row">                          
                            <div class="col-12">
                              <div class="form-group">
                                <label >Alamat</label>
                                <textarea class="form-control" rows="4"></textarea>
                              </div>
                            </div>
                        </div>
                        <hr>
                        <div class="row">                          
                            <div class="col-12">
                              <div class="form-group">
                                <label >Password</label>
                                <input type="password" class="form-control" name="">
                              </div>
                            </div>
                        </div>

                        <div class="row">                          
                            <div class="col-12">
                              <div class="form-group">
                                <label >Confrim Password</label>
                                <input type="password" class="form-control" name="">
                              </div>
                            </div>
                        </div>

                        <div class="row">                          
                            <div class="col-12">
                                <div class="form-check form-check-inline">
                                  <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
                                  <label class="form-check-label" for="inlineCheckbox1">Alumni Unpad</label>
                                </div>
                            </div>
                        </div>

                        <div class="row pt-20">                          
                            <div class="col-6">
                                <button type="submit" class="BSbtn BSbtn-lg BSbtn-outline-dark BSbtn-block float-right">Cancel</button>
                            </div>                        
                            <div class="col-6">
                                <button type="submit" class="BSbtn BSbtn-lg BSbtn-block BSbtn-info float-right">Submit</button>
                            </div>
                        </div>

                        <div class="row pt-20 text-center"> 
                            <div class="col-12">
                                <p>Sudah punya Akun? <a href="login.php">Login disini.</a></p>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script src="./assets/plugin/treeview/tree_view.js"></script>
<?php
    include "footer.php";
?>