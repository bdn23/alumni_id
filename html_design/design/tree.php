<?php
    include "header.php";
?>

    <link rel="stylesheet" href="assets/plugin/treeview/tree_view.css">
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>

    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <ul id="tree1">
                    <li><a href="#">TECH</a>
                        <ul>
                            <li>Company Maintenance</li>
                            <li><a href="#">Employees</a>
                                <ul>
                                    <li>Reports
                                        <ul>
                                            <li><a href="biography.php"> Biography</a></li>
                                            <li>Report2</li>
                                            <li>Report3</li>
                                        </ul>
                                    </li>
                                    <li>Employee Maint.</li>
                                </ul>
                            </li>
                            <li>Human Resources</li>
                        </ul>
                    </li>
                    <li>XRP
                        <ul>
                            <li>Company Maintenance</li>
                            <li>Employees
                                <ul>
                                    <li>Reports
                                        <ul>
                                            <li>Report1</li>
                                            <li>Report2</li>
                                            <li>Report3</li>
                                        </ul>
                                    </li>
                                    <li>Employee Maint.</li>
                                </ul>
                            </li>
                            <li>Human Resources</li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <script src="./assets/plugin/treeview/tree_view.js"></script>
<?php
    include "footer.php";
?>