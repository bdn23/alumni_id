<?php
    include "header.php";
?>

<link rel="stylesheet" href="assets/plugin/treeview/tree_view.css">
<!-- <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css"> -->
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<main class="position-relative">
    <!--archive header-->
    <div class="archive-header text-center">
        <div class="container">
            <h2><span class="color2">Journal</span></h2>
            <div class="breadcrumb">
                <a href="index.html" rel="nofollow">Home</a>
                <span></span>
                Journal
            </div>
            <div class="bt-1 border-color-1 mt-30 mb-20"></div>
        </div>
    </div>
    <!--main content-->
    <div class="main_content sidebar_right pb-50">
        <div class="">
            <div class="pt-20 pb-20 background-white">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4 text-center" style="margin-top: auto;margin-bottom: auto;">
                            <img src="assets/imgs/journal.png" width="80%">
                        </div>
                        <div class="col-md-8">
                            <div class="loop-grid row">
                                <article class="col-lg-6 animate-conner">
                                    <div class="post-content">
                                        <h3 class="post-title">
                                            <a href="single.html">Journal No 1</a>
                                        </h3>
                                        <div class="post-excerpt mb-25">
                                            <p><?= lorem(20) ?>...</p>
                                            <a href="single.html" class="read-more">Read More</a>
                                        </div>
                                    </div>
                                </article>
                                <article class="col-lg-6 animate-conner">
                                    <div class="post-content">
                                        <h3 class="post-title">
                                            <a href="single.html">Journal No 2</a>
                                        </h3>
                                        <div class="post-excerpt mb-25">
                                            <p><?= lorem(20) ?>...</p>
                                            <a href="single.html" class="read-more">Read More</a>
                                        </div>
                                    </div>
                                </article>
                                <article class="col-lg-6 animate-conner">
                                    <div class="post-content">
                                        <h3 class="post-title">
                                            <a href="single.html">Journal No 3</a>
                                        </h3>
                                        <div class="post-excerpt mb-25">
                                            <p><?= lorem(20) ?>...</p>
                                            <a href="single.html" class="read-more">Read More</a>
                                        </div>
                                    </div>
                                </article>
                                <article class="col-lg-6 animate-conner">
                                    <div class="post-content">
                                        <h3 class="post-title">
                                            <a href="single.html">Journal No 4</a>
                                        </h3>
                                        <div class="post-excerpt mb-25">
                                            <p><?= lorem(20) ?>...</p>
                                            <a href="single.html" class="read-more">Read More</a>
                                        </div>
                                    </div>
                                </article>
                            </div>
                        </div>
                    </div>
                    <div class="bt-1 border-color-1 mt-30 mb-20"></div>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="sidebar-widget widget-about mb-10 pt-30 pr-20 pb-30 pl-20 background12 border-radius-5 overflow-auto">
                                <h5 class="text-info">Index</h5>
                                <small>
                                    <ul id="tree1">
                                        <?php 
                                            for ($i=1; $i < 17; $i++) { 
                                        ?>

                                            <li><a href="#">Journal No <?= $i ?></a>
                                                <ul>
                                                    <li>Journal No <?= $i ?> A</li>
                                                    <li><a href="#">Journal No <?= $i ?> B</a>
                                                        <ul>
                                                            <li><a href="#">Pasal</a>
                                                                <ul>
                                                                    <li><a href="biography.php"> Pasal 1</a></li>
                                                                    <li>Pasal 2</li>
                                                                    <li>Pasal 3</li>
                                                                </ul>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li>Journal No <?= $i ?> B</li>
                                                </ul>
                                            </li>
                                        <?php   
                                            }
                                        ?>
                                    </ul>
                                </small>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="widget-taber-content background-white pt-30 pb-30 pl-30 pr-30 border-radius-5">
                                <div class="widget-header position-relative mb-20 pb-10">
                                    <h5 class="widget-title mb-10">Journal No 1 A</h5>
                                    <div class="bt-1 border-color-1"></div>
                                </div>
                                <div class="post-block-list post-module-1">
                                    <p><?= lorem(50) ?></p>
                                </div>
                                <div class="row mt-35">
                                    <div class="col-6">
                                        <button class="BSbtn BSbtn-block BSbtn-success"><i class="fas fa-download"></i> Download</button>
                                    </div>
                                    <div class="col-6">
                                        <button class="BSbtn BSbtn-block BSbtn-outline-dark"><i class="fas fa-print"></i> Print</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            
                            <div class="widget-area">
                                <!--taber-->
                                <div class="sidebar-widget widget-taber mb-30">
                                    <div class="widget-taber-content background-white pt-30 pb-30 pl-30 pr-30 border-radius-5">
                                        <nav class="tab-nav float-none mb-20">
                                            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                                <a class="nav-item nav-link active" id="nav-popular-tab" data-toggle="tab" href="#nav-popular" role="tab" aria-controls="nav-popular" aria-selected="true">Berita Terbaru</a>
                                            </div>
                                        </nav>
                                        <div class="tab-content">
                                            <!--Tab Popular-->
                                            <div class="tab-pane fade show active" id="nav-popular" role="tabpanel" aria-labelledby="nav-popular-tab">
                                                <div class="row">
                                                    <div class="col-md-12 mb-20">  
                                                        <div class="post-thumb position-relative thumb-overlay mb-15">
                                                            <div class="img-hover-slide border-radius-5 position-relative" style="background-image: url('assets/imgs/news-1.jpg')">
                                                                <a class="img-link" href="single.html"></a>
                                                            </div>
                                                        </div>
                                                        <div class="post-content">
                                                            <h4 class="post-title">
                                                                <a href="single.html">Vaksin Virus Covid-19 Sudah Ditemukan</a>
                                                            </h4>
                                                            <div class="entry-meta meta-1 font-small color-grey mt-15 mb-15">
                                                                <span class="post-on"><i class="ti-marker-alt"></i>25 April 2020</span>
                                                                <span class="time-reading"><i class="ti-timer"></i>10 mins read</span>
                                                                <span class="hit-count"><i class="ti-bolt"></i> 159k Views</span>
                                                            </div>
                                                            <p class="font-medium">These people envy me for having a lifestyle they don’t have, but the truth is, sometimes I envy their lifestyle instead. Struggling to sell one multi-million dollar home currently.</p>
                                                            <a class="readmore-btn font-small text-uppercase font-weight-ultra" href="single.html">Read More<i class="ti-arrow-right ml-5"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="post-block-list post-module-1">
                                                    <ul class="list-post">
                                                        <li class="mb-30">
                                                            <div class="d-flex">
                                                                <div class="post-thumb d-flex mr-15 border-radius-5 img-hover-scale">
                                                                    <a href="single.html">
                                                                        <img src="assets/imgs/news-2.jpg" alt="">
                                                                    </a>
                                                                </div>
                                                                <div class="post-content media-body">
                                                                    <div class="entry-meta meta-0 mb-10">
                                                                        <a href="category.html"><span class="post-in background5 color-white font-small">World</span></a>
                                                                    </div>
                                                                    <h6 class="post-title mb-10 text-limit-2-row">Inilah Visi Misi Dr. Ary Zulfikar Calon Ketua IKA Universitas Padjadjaran</h6>
                                                                    <div class="entry-meta meta-1 font-x-small color-grey">
                                                                        <span class="post-on">25 April</span>
                                                                        <span class="hit-count has-dot">26k Views</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </li>
                                                        <li class="mb-30">
                                                            <div class="d-flex">
                                                                <div class="post-thumb d-flex mr-15 border-radius-5 img-hover-scale">
                                                                    <a href="single.html">
                                                                        <img src="assets/imgs/news-3.jpg" alt="">
                                                                    </a>
                                                                </div>
                                                                <div class="post-content media-body">
                                                                    <div class="entry-meta meta-0 mb-10">
                                                                        <a href="category.html"><span class="post-in background7 color-white font-small">Films</span></a>
                                                                    </div>
                                                                    <h6 class="post-title mb-10 text-limit-2-row">Aji Mumpung Jualan Surat Bebas Corona Palsu di Tengah Pandemi</h6>
                                                                    <div class="entry-meta meta-1 font-x-small color-grey mt-10">
                                                                        <span class="post-on">25 April</span>
                                                                        <span class="hit-count has-dot">37k Views</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </li>
                                                        <li class="mb-30">
                                                            <div class="d-flex">
                                                                <div class="post-thumb d-flex mr-15 border-radius-5 img-hover-scale">
                                                                    <a href="single.html">
                                                                        <img src="assets/imgs/news-4.jpg" alt="">
                                                                    </a>
                                                                </div>
                                                                <div class="post-content media-body">
                                                                    <div class="entry-meta meta-0 mb-10">
                                                                        <a href="category.html"><span class="post-in background2 color-white font-small">Travel</span></a>
                                                                    </div>
                                                                    <h6 class="post-title mb-10 text-limit-2-row">Bantuan Kasur Lipat untuk Ruang Perawatan Tambahan</h6>
                                                                    <div class="entry-meta meta-1 font-x-small color-grey mt-10">
                                                                        <span class="post-on">25 April</span>
                                                                        <span class="hit-count has-dot">54k Views</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <div class="d-flex">
                                                                <div class="post-thumb d-flex mr-15 border-radius-5 img-hover-scale">
                                                                    <a href="single.html">
                                                                        <img src="assets/imgs/news-5.jpg" alt="">
                                                                    </a>
                                                                </div>
                                                                <div class="post-content media-body">
                                                                    <div class="entry-meta meta-0 mb-10">
                                                                        <a href="category.html"><span class="post-in background3 color-white font-small">Beauty</span></a>
                                                                    </div>
                                                                    <h6 class="post-title mb-10 text-limit-2-row">Bertambah 11, Positif Corona di Bengkulu Jadi 53 Kasus</h6>
                                                                    <div class="entry-meta meta-1 font-x-small color-grey mt-10">
                                                                        <span class="post-on">25 April</span>
                                                                        <span class="hit-count has-dot">126k Views</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Ads -->
                                <div class="sidebar-widget widget-ads mb-30 text-center">
                                    <a href="https://vimeo.com/333834999" class="play-video" data-animate="zoomIn" data-duration="1.5s" data-delay="0.1s">
                                        <img class="d-inline-block" src="http://via.placeholder.com/432x200" alt="">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="recent-area pt-50 pb-50">
        <div class="container">

        </div>
    </div>
</main>

    <script src="./assets/plugin/treeview/tree_view.js"></script>
<?php
    include "footer.php";
?>