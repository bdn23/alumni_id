<?php
    include "header.php";
?>

<main class="position-relative">
    <!--archive header-->
    <div class="archive-header text-center">
        <div class="container">
            <h2><span class="color2">Profesi & Jasa Alumni</span></h2>
            <div class="breadcrumb">
                <a href="index.html" rel="nofollow">Home</a>
                <span></span>
                Profesi & Jasa Alumni
            </div>
            <div class="bt-1 border-color-1 mt-30 mb-20"></div>
        </div>
    </div>
    <!--main content-->
</main>

 <!--Featured post Start-->
<div class="home-featured mb-50 mt-50">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="featured-slider-1 border-radius-10">
                    <div class="featured-slider-1-items">
                        <div class="slider-single">
                            <div class="row no-gutters">
                                <div class="col-lg-6 col-md-12 order-lg-1 order-2 align-center-vertical">
                                    <div class="slider-caption">
                                        <div class="entry-meta meta-0 mb-25">
                                            <a href="category.html"><span class="post-in background1 color-white font-small bg-success">Kesehatan</span></a>
                                        </div>
                                        <h2 class="post-title"><a href="#">Klinik Gigi</a></h2>
                                        <div class="entry-meta meta-1 font-small color-grey mt-20 mb-20"> 
                                            <p>
                                                <?= lorem(16) ?>
                                            </p>
                                        </div>
                                        <p class="excerpt font-small mt-25 mb-25">
                                            <small>
                                                <table class="table table-sm borderless font-small">
                                                    <tr>
                                                        <td><i class="fas fa-map-marker-alt"></i></td>
                                                        <td>Jalan Panjang Lebar RT 01 / RW 02 <br> Ciawi, Bogor</td>
                                                    </tr>
                                                    <tr>
                                                        <td><i class="fas fa-phone"></i></td>
                                                        <td>0857 11200 377</td>
                                                    </tr>
                                                </table>
                                            </small>
                                        </p>
                                        <p>
                                            <button class="BSbtn BSbtn-outline-info SBbtn-sm">Selengkapnya</button>
                                        </p>
                                    </div>
                                </div>
                                <div class="slider-img col-lg-6 order-lg-2 order-1 col-md-12">
                                    <div class="img-hover-scale">
                                        <a href="single.html">
                                            <img src="assets/imgs/pnj-1.jpg" alt="post-slider">
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="slider-single">
                            <div class="row no-gutters">
                                <div class="col-lg-6 col-md-12 order-lg-1 order-2 align-center-vertical">
                                    <div class="slider-caption">
                                        <div class="entry-meta meta-0 mb-25">
                                            <a href="category.html"><span class="post-in background1 color-white font-small bg-success">Bisnis</span></a>
                                        </div>
                                        <h2 class="post-title"><a href="#">Konsultan Bisnis</a></h2>
                                        <div class="entry-meta meta-1 font-small color-grey mt-20 mb-20"> 
                                            <p>
                                                <?= lorem(16) ?>
                                            </p>
                                        </div>
                                        <p class="excerpt font-small mt-25 mb-25">
                                            <small>
                                                <table class="table table-sm borderless font-small">
                                                    <tr>
                                                        <td><i class="fas fa-map-marker-alt"></i></td>
                                                        <td>Jalan Panjang Lebar RT 01 / RW 02 <br> Ciawi, Bogor</td>
                                                    </tr>
                                                    <tr>
                                                        <td><i class="fas fa-phone"></i></td>
                                                        <td>0857 11200 377</td>
                                                    </tr>
                                                </table>
                                            </small>
                                        </p>
                                        <p>
                                            <button class="BSbtn BSbtn-outline-info SBbtn-sm">Selengkapnya</button>
                                        </p>
                                    </div>
                                </div>
                                <div class="slider-img col-lg-6 order-lg-2 order-1 col-md-12">
                                    <div class="img-hover-scale">
                                        <a href="single.html">
                                            <img src="assets/imgs/pnj-2.jpg" alt="post-slider">
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="slider-single">
                            <div class="row no-gutters">
                                <div class="col-lg-6 col-md-12 order-lg-1 order-2 align-center-vertical">
                                    <div class="slider-caption">
                                        <div class="entry-meta meta-0 mb-25">
                                            <a href="category.html"><span class="post-in background1 color-white font-small bg-success">Services</span></a>
                                        </div>
                                        <h2 class="post-title"><a href="#">Jasa Pindah Rumah</a></h2>
                                        <div class="entry-meta meta-1 font-small color-grey mt-20 mb-20"> 
                                            <p>
                                                <?= lorem(16) ?>
                                            </p>
                                        </div>
                                        <p class="excerpt font-small mt-25 mb-25">
                                            <small>
                                                <table class="table table-sm borderless font-small">
                                                    <tr>
                                                        <td><i class="fas fa-map-marker-alt"></i></td>
                                                        <td>Jalan Panjang Lebar RT 01 / RW 02 <br> Ciawi, Bogor</td>
                                                    </tr>
                                                    <tr>
                                                        <td><i class="fas fa-phone"></i></td>
                                                        <td>0857 11200 377</td>
                                                    </tr>
                                                </table>
                                            </small>
                                        </p>
                                        <p>
                                            <button class="BSbtn BSbtn-outline-info SBbtn-sm">Selengkapnya</button>
                                        </p>
                                    </div>
                                </div>
                                <div class="slider-img col-lg-6 order-lg-2 order-1 col-md-12">
                                    <div class="img-hover-scale">
                                        <a href="single.html">
                                            <img src="assets/imgs/pnj-3.jpg" alt="post-slider">
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-md-12">
                            <div class="arrow-cover"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--Featured post End-->
<div class="pt-50 pb-0 background-white">
    <div class="main_content sidebar_right mt-5 pb-50">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-12 col-sm-12">
                    <!--loop-list-->

                    <div class="loop-grid row">
                        <article class="col-lg-6 mb-50 animate-conner">
                            <div class="post-thumb d-flex mb-30 border-radius-5 img-hover-scale animate-conner-box">
                                <a href="single.html">
                                    <img src="assets/imgs/pnj-1.jpg" alt="">
                                </a>
                            </div>
                            <div class="post-content">
                                <div class="entry-meta meta-0 font-small mb-15">
                                    <a href="category.html"><span class="post-cat background2 color-white">Kesehatan</span></a>
                                </div>
                                <h3 class="post-title">
                                    <a href="single.html">Klinik Gigi</a>
                                </h3>
                                <div class="entry-meta meta-1 font-small color-grey mt-15 mb-15">
                                    <span class="post-by">By <a href="author.html">Melody Nurramdhani</a></span>
                                </div>
                                <div class="post-excerpt mb-25">
                                    <p>
                                        <?= lorem(16) ?>
                                    </p>
                                    <small>
                                        <table class="table table-sm borderless">
                                            <tr>
                                                <td><i class="fas fa-map-marker-alt"></i></td>
                                                <td>Jalan Panjang Lebar RT 01 / RW 02 <br> Ciawi, Bogor</td>
                                            </tr>
                                            <tr>
                                                <td><i class="fas fa-phone"></i></td>
                                                <td>0857 11200 377</td>
                                            </tr>
                                        </table>
                                    </small>
                                </div>
                            </div>
                        </article>
                        <article class="col-lg-6 mb-50 animate-conner">
                            <div class="post-thumb d-flex mb-30 border-radius-5 img-hover-scale animate-conner-box">
                                <a href="single.html">
                                    <img src="assets/imgs/pnj-2.jpg" alt="">
                                </a>
                            </div>
                            <div class="post-content">
                                <div class="entry-meta meta-0 font-small mb-15">
                                    <a href="category.html"><span class="post-cat background3 color-white">Produk</span></a>
                                </div>
                                <h3 class="post-title">
                                    <a href="single.html">Konsultan Bisnis</a>
                                </h3>
                                <div class="entry-meta meta-1 font-small color-grey mt-15 mb-15">
                                    <span class="post-by">By <a href="author.html">Syafira Razak</a></span>
                                </div>
                                <div class="post-excerpt mb-25">
                                    <p>
                                        <?= lorem(16) ?>
                                    </p>
                                    <small>
                                        <table class="table table-sm borderless">
                                            <tr>
                                                <td><i class="fas fa-map-marker-alt"></i></td>
                                                <td>Jalan Panjang Lebar RT 01 / RW 02 <br> Ciawi, Bogor</td>
                                            </tr>
                                            <tr>
                                                <td><i class="fas fa-phone"></i></td>
                                                <td>0857 11200 377</td>
                                            </tr>
                                        </table>
                                    </small>
                                </div>
                            </div>
                        </article>
                        <article class="col-lg-6 mb-50 animate-conner">
                            <div class="post-thumb d-flex mb-30 border-radius-5 img-hover-scale animate-conner-box">
                                <a href="single.html">
                                    <img src="assets/imgs/pnj-3.jpg" alt="">
                                </a>
                            </div>
                            <div class="post-content">
                                <div class="entry-meta meta-0 font-small mb-15">
                                    <a href="category.html"><span class="post-cat background2 color-white">Jasa</span></a>
                                </div>
                                <h3 class="post-title">
                                    <a href="single.html">Jasa Pindah Rumah</a>
                                </h3>
                                <div class="entry-meta meta-1 font-small color-grey mt-15 mb-15">
                                    <span class="post-by">By <a href="author.html">Ernest Prakasa</a></span>
                                </div>
                                <div class="post-excerpt mb-25">
                                    <p>
                                        <?= lorem(16) ?>
                                    </p>
                                    <small>
                                        <table class="table table-sm borderless">
                                            <tr>
                                                <td><i class="fas fa-map-marker-alt"></i></td>
                                                <td>Jalan Panjang Lebar RT 01 / RW 02 <br> Ciawi, Bogor</td>
                                            </tr>
                                            <tr>
                                                <td><i class="fas fa-phone"></i></td>
                                                <td>0857 11200 377</td>
                                            </tr>
                                        </table>
                                    </small>
                                </div>
                            </div>
                        </article>
                        <article class="col-lg-6 mb-50 animate-conner">
                            <div class="post-thumb d-flex mb-30 border-radius-5 img-hover-scale animate-conner-box">
                                <a href="single.html">
                                    <img src="assets/imgs/pnj-4.jpg" alt="">
                                </a>
                            </div>
                            <div class="post-content">
                                <div class="entry-meta meta-0 font-small mb-15">
                                    <a href="category.html"><span class="post-cat background2 color-white">Jasa</span></a>
                                </div>
                                <h3 class="post-title">
                                    <a href="single.html">Software House</a>
                                </h3>
                                <div class="entry-meta meta-1 font-small color-grey mt-15 mb-15">
                                    <span class="post-by">By <a href="author.html">Soleh Solehun</a></span>
                                </div>
                                <div class="post-excerpt mb-25">
                                    <p>
                                        <?= lorem(16) ?>
                                    </p>
                                    <small>
                                        <table class="table table-sm borderless">
                                            <tr>
                                                <td><i class="fas fa-map-marker-alt"></i></td>
                                                <td>Jalan Panjang Lebar RT 01 / RW 02 <br> Ciawi, Bogor</td>
                                            </tr>
                                            <tr>
                                                <td><i class="fas fa-phone"></i></td>
                                                <td>0857 11200 377</td>
                                            </tr>
                                        </table>
                                    </small>
                                </div>
                            </div>
                        </article>
                    </div>
                    <hr class="mt-0">
                </div>
                <div class="col-lg-4 col-md-12 col-sm-12 primary-sidebar sticky-sidebar">

                    <div class="widget-area">
                        <!--taber-->
                        <div class="sidebar-widget widget-taber mb-30">
                            <div class="widget-taber-content background-white pt-30 pb-30 pl-30 pr-30 border-radius-5">
                                <nav class="tab-nav float-none mb-20">
                                    <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                        <a class="nav-item nav-link active" id="nav-popular-tab" data-toggle="tab" href="#nav-popular" role="tab" aria-controls="nav-popular" aria-selected="true">Berita Terbaru</a>
                                    </div>
                                </nav>
                                <div class="tab-content">
                                    <!--Tab Popular-->
                                    <div class="tab-pane fade show active" id="nav-popular" role="tabpanel" aria-labelledby="nav-popular-tab">
                                        <div class="row">
                                            <div class="col-md-12 mb-20">  
                                                <div class="post-thumb position-relative thumb-overlay mb-15">
                                                    <div class="img-hover-slide border-radius-5 position-relative" style="background-image: url('assets/imgs/news-1.jpg')">
                                                        <a class="img-link" href="single.html"></a>
                                                    </div>
                                                </div>
                                                <div class="post-content">
                                                    <h4 class="post-title">
                                                        <a href="single.html">Vaksin Virus Covid-19 Sudah Ditemukan</a>
                                                    </h4>
                                                    <div class="entry-meta meta-1 font-small color-grey mt-15 mb-15">
                                                        <span class="post-on"><i class="ti-marker-alt"></i>25 April 2020</span>
                                                        <span class="time-reading"><i class="ti-timer"></i>10 mins read</span>
                                                        <span class="hit-count"><i class="ti-bolt"></i> 159k Views</span>
                                                    </div>
                                                    <p class="font-small">These people envy me for having a lifestyle they don’t have, but the truth is, sometimes I envy their lifestyle instead. Struggling to sell one multi-million dollar home currently.</p>
                                                    <a class="readmore-btn font-small text-uppercase font-weight-ultra" href="single.html">Baca Selengkapnya<i class="ti-arrow-right ml-5"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="post-block-list post-module-1">
                                            <ul class="list-post">
                                                <li class="mb-30">
                                                    <div class="d-flex">
                                                        <div class="post-thumb d-flex mr-15 border-radius-5 img-hover-scale">
                                                            <a href="single.html">
                                                                <img src="assets/imgs/news-2.jpg" alt="">
                                                            </a>
                                                        </div>
                                                        <div class="post-content media-body">
                                                            <div class="entry-meta meta-0 mb-10">
                                                                <a href="category.html"><span class="post-in background5 color-white font-small">World</span></a>
                                                            </div>
                                                            <h6 class="post-title mb-10 text-limit-2-row">Inilah Visi Misi Dr. Ary Zulfikar Calon Ketua IKA Universitas Padjadjaran</h6>
                                                            <div class="entry-meta meta-1 font-x-small color-grey">
                                                                <span class="post-on">25 April</span>
                                                                <span class="hit-count has-dot">26k Views</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="mb-30">
                                                    <div class="d-flex">
                                                        <div class="post-thumb d-flex mr-15 border-radius-5 img-hover-scale">
                                                            <a href="single.html">
                                                                <img src="assets/imgs/news-3.jpg" alt="">
                                                            </a>
                                                        </div>
                                                        <div class="post-content media-body">
                                                            <div class="entry-meta meta-0 mb-10">
                                                                <a href="category.html"><span class="post-in background7 color-white font-small">Films</span></a>
                                                            </div>
                                                            <h6 class="post-title mb-10 text-limit-2-row">Aji Mumpung Jualan Surat Bebas Corona Palsu di Tengah Pandemi</h6>
                                                            <div class="entry-meta meta-1 font-x-small color-grey mt-10">
                                                                <span class="post-on">25 April</span>
                                                                <span class="hit-count has-dot">37k Views</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="mb-30">
                                                    <div class="d-flex">
                                                        <div class="post-thumb d-flex mr-15 border-radius-5 img-hover-scale">
                                                            <a href="single.html">
                                                                <img src="assets/imgs/news-4.jpg" alt="">
                                                            </a>
                                                        </div>
                                                        <div class="post-content media-body">
                                                            <div class="entry-meta meta-0 mb-10">
                                                                <a href="category.html"><span class="post-in background2 color-white font-small">Travel</span></a>
                                                            </div>
                                                            <h6 class="post-title mb-10 text-limit-2-row">Bantuan Kasur Lipat untuk Ruang Perawatan Tambahan</h6>
                                                            <div class="entry-meta meta-1 font-x-small color-grey mt-10">
                                                                <span class="post-on">25 April</span>
                                                                <span class="hit-count has-dot">54k Views</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="d-flex">
                                                        <div class="post-thumb d-flex mr-15 border-radius-5 img-hover-scale">
                                                            <a href="single.html">
                                                                <img src="assets/imgs/news-5.jpg" alt="">
                                                            </a>
                                                        </div>
                                                        <div class="post-content media-body">
                                                            <div class="entry-meta meta-0 mb-10">
                                                                <a href="category.html"><span class="post-in background3 color-white font-small">Beauty</span></a>
                                                            </div>
                                                            <h6 class="post-title mb-10 text-limit-2-row">Bertambah 11, Positif Corona di Bengkulu Jadi 53 Kasus</h6>
                                                            <div class="entry-meta meta-1 font-x-small color-grey mt-10">
                                                                <span class="post-on">25 April</span>
                                                                <span class="hit-count has-dot">126k Views</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="row mt-20 mb-50 author-bio ">
                        <div class="col-md-2">
                            <label>Kategori</label>
                            <input type="text" class="form-control" name="">
                        </div>
                        <div class="col-md-5">
                            <label>Nama</label>
                            <input type="text" class="form-control" name="">
                        </div>
                        <div class="col-md-3">
                            <label>Kota</label>
                            <input type="text" class="form-control" name="">
                        </div>
                        <div class="col-md-2">
                            <button class="BSbtn BSbtn-lg BSbtn-outline-info mt-30"><i class="fas fa-search"></i> Search</button>
                        </div>
                    </div>
                    <div class="loop-grid row">
                        <?php 
                            for ($i=0; $i < 3; $i++) { 
                        ?>
                            <div class="col-md-6">
                                <article class="row mb-50">
                                    <div class="col-md-6">
                                        <div class="post-thumb position-relative thumb-overlay mr-20">
                                            <div class="img-hover-slide border-radius-5 position-relative" style="background-image: url('assets/imgs/pnj-1.jpg')">
                                                <a class="img-link" href="single.html"></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 align-center-vertical">
                                        <div class="post-content">
                                            <div class="entry-meta meta-0 font-small mb-15">
                                                <a href="category.html"><span class="post-cat background2 color-white">Kesehatan</span></a>
                                            </div>
                                            <h4 class="post-title">
                                                <a href="single.html">Klinik Gigi</a>
                                            </h4>
                                            <div class="entry-meta meta-1 font-small color-grey mt-15 mb-15">
                                                <span class="post-by">By <a href="author.html">Melody Nurramdhani</a></span>
                                            </div>
                                            <div class="post-excerpt mb-25">
                                                <p>
                                                    <?= lorem(10) ?>
                                                </p>
                                                <small>
                                                    <table class="table table-sm borderless">
                                                        <tr>
                                                            <td><i class="fas fa-map-marker-alt"></i></td>
                                                            <td>Jalan Panjang Lebar RT 01 / RW 02 <br> Ciawi, Bogor</td>
                                                        </tr>
                                                        <tr>
                                                            <td><i class="fas fa-phone"></i></td>
                                                            <td>0857 11200 377</td>
                                                        </tr>
                                                    </table>
                                                </small>
                                            </div>
                                            <a class="readmore-btn font-small text-uppercase font-weight-ultra" href="single.html">Read More<i class="ti-arrow-right ml-5 transition-02s"></i></a>
                                        </div>
                                    </div>
                                </article>
                            </div>
                            <div class="col-md-6">
                                <article class="row mb-50">
                                    <div class="col-md-6">
                                        <div class="post-thumb position-relative thumb-overlay mr-20">
                                            <div class="img-hover-slide border-radius-5 position-relative" style="background-image: url('assets/imgs/pnj-2.jpg')">
                                                <a class="img-link" href="single.html"></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 align-center-vertical">
                                        <div class="post-content">
                                            <div class="entry-meta meta-0 font-small mb-15">
                                                <a href="category.html"><span class="post-cat background2 color-white">Bisnis</span></a>
                                            </div>
                                            <h4 class="post-title">
                                                <a href="single.html">Konsultan Bisnis</a>
                                            </h4>
                                            <div class="entry-meta meta-1 font-small color-grey mt-15 mb-15">
                                                <span class="post-by">By <a href="author.html">Melody Nurramdhani</a></span>
                                            </div>
                                            <div class="post-excerpt mb-25">
                                                <p>
                                                    <?= lorem(10) ?>
                                                </p>
                                                <small>
                                                    <table class="table table-sm borderless">
                                                        <tr>
                                                            <td><i class="fas fa-map-marker-alt"></i></td>
                                                            <td>Jalan Panjang Lebar RT 01 / RW 02 <br> Ciawi, Bogor</td>
                                                        </tr>
                                                        <tr>
                                                            <td><i class="fas fa-phone"></i></td>
                                                            <td>0857 11200 377</td>
                                                        </tr>
                                                    </table>
                                                </small>
                                            </div>
                                            <a class="readmore-btn font-small text-uppercase font-weight-ultra" href="single.html">Read More<i class="ti-arrow-right ml-5 transition-02s"></i></a>
                                        </div>
                                    </div>
                                </article>
                            </div>
                            <div class="col-md-6">
                                <article class="row mb-50">
                                    <div class="col-md-6">
                                        <div class="post-thumb position-relative thumb-overlay mr-20">
                                            <div class="img-hover-slide border-radius-5 position-relative" style="background-image: url('assets/imgs/pnj-3.jpg')">
                                                <a class="img-link" href="single.html"></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 align-center-vertical">
                                        <div class="post-content">
                                            <div class="entry-meta meta-0 font-small mb-15">
                                                <a href="category.html"><span class="post-cat background2 color-white">Jasa</span></a>
                                            </div>
                                            <h4 class="post-title">
                                                <a href="single.html">Jasa Pindah Rumah</a>
                                            </h4>
                                            <div class="entry-meta meta-1 font-small color-grey mt-15 mb-15">
                                                <span class="post-by">By <a href="author.html">Melody Nurramdhani</a></span>
                                            </div>
                                            <div class="post-excerpt mb-25">
                                                <p>
                                                    <?= lorem(10) ?>
                                                </p>
                                                <small>
                                                    <table class="table table-sm borderless">
                                                        <tr>
                                                            <td><i class="fas fa-map-marker-alt"></i></td>
                                                            <td>Jalan Panjang Lebar RT 01 / RW 02 <br> Ciawi, Bogor</td>
                                                        </tr>
                                                        <tr>
                                                            <td><i class="fas fa-phone"></i></td>
                                                            <td>0857 11200 377</td>
                                                        </tr>
                                                    </table>
                                                </small>
                                            </div>
                                            <a class="readmore-btn font-small text-uppercase font-weight-ultra" href="single.html">Read More<i class="ti-arrow-right ml-5 transition-02s"></i></a>
                                        </div>
                                    </div>
                                </article>
                            </div>
                            <div class="col-md-6">
                                <article class="row mb-50">
                                    <div class="col-md-6">
                                        <div class="post-thumb position-relative thumb-overlay mr-20">
                                            <div class="img-hover-slide border-radius-5 position-relative" style="background-image: url('assets/imgs/pnj-4.jpg')">
                                                <a class="img-link" href="single.html"></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 align-center-vertical">
                                        <div class="post-content">
                                            <div class="entry-meta meta-0 font-small mb-15">
                                                <a href="category.html"><span class="post-cat background2 color-white">Teknologi</span></a>
                                            </div>
                                            <h4 class="post-title">
                                                <a href="single.html">Software House</a>
                                            </h4>
                                            <div class="entry-meta meta-1 font-small color-grey mt-15 mb-15">
                                                <span class="post-by">By <a href="author.html">Melody Nurramdhani</a></span>
                                            </div>
                                            <div class="post-excerpt mb-25">
                                                <p>
                                                    <?= lorem(10) ?>
                                                </p>
                                                <small>
                                                    <table class="table table-sm borderless">
                                                        <tr>
                                                            <td><i class="fas fa-map-marker-alt"></i></td>
                                                            <td>Jalan Panjang Lebar RT 01 / RW 02 <br> Ciawi, Bogor</td>
                                                        </tr>
                                                        <tr>
                                                            <td><i class="fas fa-phone"></i></td>
                                                            <td>0857 11200 377</td>
                                                        </tr>
                                                    </table>
                                                </small>
                                            </div>
                                            <a class="readmore-btn font-small text-uppercase font-weight-ultra" href="single.html">Read More<i class="ti-arrow-right ml-5 transition-02s"></i></a>
                                        </div>
                                    </div>
                                </article>
                            </div>
                        <?php 
                            }
                        ?>
                    </div>
                    <!--pagination-->
                    <div class="pagination-area pt-30 text-center bt-1 border-color-1">
                        <div class="container">
                            <div class="row">
                                <div class="col-xl-12">
                                    <div class="single-wrap d-flex justify-content-center">
                                        <nav aria-label="Page navigation example">
                                            <ul class="pagination justify-content-start">
                                                <li class="page-item"><a class="page-link" href="#"><i class="flaticon-left"></i> </a></li>
                                                <li class="page-item active"><a class="page-link" href="#">01</a></li>
                                                <li class="page-item"><a class="page-link" href="#">02</a></li>
                                                <li class="page-item"><a class="page-link" href="#">03</a></li>
                                                <li class="page-item"><a class="page-link" href="#"><i class="flaticon-right"></i> </a></li>
                                            </ul>
                                        </nav>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
    include "footer.php";
?>