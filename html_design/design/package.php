<?php
    include "header.php";
?>

<main class="position-relative">
    <!--archive header-->
    <div class="archive-header text-center">
        <div class="container">
            <h2><span class="color2">Paket Berlangganan</span></h2>
            <div class="breadcrumb">
                <a href="index.html" rel="nofollow">Home</a>
                <span></span>
                Paket Berlangganan
            </div>
            <div class="bt-1 border-color-1 mt-30 mb-20"></div>
        </div>
    </div>
    <!--main content-->
</main>
 <!--Featured post Start-->
<div class="home-featured mb-50">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="featured-slider-1 border-radius-10">
                    <div class="featured-slider-1-items">
                        <div class="slider-single">
                            <div class="row no-gutters">
                                <div class="col-lg-6 col-md-12 order-lg-1 order-2 align-center-vertical">
                                    <div class="slider-caption">
                                        <div class="entry-meta meta-0 mb-25">
                                            <a href="category.html"><span class="post-in background1 color-white font-small">Best Seller</span></a>
                                        </div>
                                        <h2 class="post-title"><a href="#">Paket Lengkap</a></h2>
                                        <div class="entry-meta meta-1 font-small color-grey mt-20 mb-20">
                                            <h4><span class="text-success">Rp. 1.500.000</span><small class="text-mute">/Bulan</small></h4>
                                        </div>
                                        <p class="excerpt font-medium mt-25 mb-25">
                                            Berlaku untuk 5 User, <br>
                                            <?= lorem(20) ?>
                                        </p>
                                        <p>
                                            <button class="BSbtn BSbtn-outline-success SBbtn-sm">Beli Paket</button>
                                        </p>
                                    </div>
                                </div>
                                <div class="slider-img col-lg-6 order-lg-2 order-1 col-md-12">
                                    <div class="img-hover-scale">
                                        <a href="single.html">
                                            <img src="assets/imgs/pay-1.jpg" alt="post-slider">
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="slider-single">
                            <div class="row no-gutters">
                                <div class="col-lg-6 col-md-12 order-lg-1 order-2 align-center-vertical">
                                    <div class="slider-caption">
                                        <div class="entry-meta meta-0 mb-25">
                                            <a href="category.html"><span class="post-in background1 color-white font-small">Best Seller</span></a>
                                        </div>
                                        <h2 class="post-title"><a href="#">Paket Journal</a></h2>
                                        <div class="entry-meta meta-1 font-small color-grey mt-20 mb-20">
                                            <h4><span class="text-success">Rp. 1.000.000</span><small class="text-mute">/Bulan</small></h4>
                                        </div>
                                        <p class="excerpt font-medium mt-25 mb-25">
                                            Berlaku untuk 5 User, <br>
                                            <?= lorem(20) ?>
                                        </p>
                                        <p>
                                            <button class="BSbtn BSbtn-outline-success SBbtn-sm">Beli Paket</button>
                                        </p>
                                    </div>
                                </div>
                                <div class="slider-img col-lg-6 order-lg-2 order-1 col-md-12">
                                    <div class="img-hover-scale">
                                        <a href="single.html">
                                            <img src="assets/imgs/pay-2.jpg" alt="post-slider">
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="slider-single">
                            <div class="row no-gutters">
                                <div class="col-lg-6 col-md-12 order-lg-1 order-2 align-center-vertical">
                                    <div class="slider-caption">
                                        <div class="entry-meta meta-0 mb-25">
                                            <a href="category.html"><span class="post-in background1 color-white font-small">Best Seller</span></a>
                                        </div>
                                        <h2 class="post-title"><a href="#">Paket Undang-Undang</a></h2>
                                        <div class="entry-meta meta-1 font-small color-grey mt-20 mb-20">
                                            <h4><span class="text-success">Rp. 1.000.000</span><small class="text-mute">/Bulan</small></h4>
                                        </div>
                                        <p class="excerpt font-medium mt-25 mb-25">
                                            Berlaku untuk 5 User, <br>
                                            <?= lorem(20) ?>
                                        </p>
                                        <p>
                                            <button class="BSbtn BSbtn-outline-success SBbtn-sm">Beli Paket</button>
                                        </p>
                                    </div>
                                </div>
                                <div class="slider-img col-lg-6 order-lg-2 order-1 col-md-12">
                                    <div class="img-hover-scale">
                                        <a href="single.html">
                                            <img src="assets/imgs/pay-3.jpg" alt="post-slider">
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-md-12">
                            <div class="arrow-cover"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--Featured post End-->

    <!--  Recent Articles start -->
    <div class="recent-area pt-50 pb-50">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-12 col-sm-12">
                    <!--loop-list-->

                    <div class="widget-header position-relative mb-30">
                        <h5 class="widget-title mb-30 text-uppercase color1 font-weight-ultra">Profesi & Jasa Alumni</h5>
                        <div class="letter-background">P&J</div>
                    </div>
                    <div class="loop-grid row">
                        <article class="col-lg-6 mb-50 animate-conner">
                            <div class="post-thumb d-flex mb-30 border-radius-5 img-hover-scale animate-conner-box">
                                <a href="single.html">
                                    <img src="assets/imgs/pnj-1.jpg" alt="">
                                </a>
                            </div>
                            <div class="post-content">
                                <div class="entry-meta meta-0 font-small mb-15">
                                    <a href="category.html"><span class="post-cat background2 color-white">Jasa</span></a>
                                </div>
                                <h3 class="post-title">
                                    <a href="single.html">Klinik Gigi</a>
                                </h3>
                                <div class="entry-meta meta-1 font-small color-grey mt-15 mb-15">
                                    <span class="post-by">By <a href="author.html">Melody Nurramdhani</a></span>
                                </div>
                                <div class="post-excerpt mb-25">
                                    <p><?= lorem(20) ?>...</p>
                                </div>
                            </div>
                        </article>
                        <article class="col-lg-6 mb-50 animate-conner">
                            <div class="post-thumb d-flex mb-30 border-radius-5 img-hover-scale animate-conner-box">
                                <a href="single.html">
                                    <img src="assets/imgs/pnj-2.jpg" alt="">
                                </a>
                            </div>
                            <div class="post-content">
                                <div class="entry-meta meta-0 font-small mb-15">
                                    <a href="category.html"><span class="post-cat background3 color-white">Produk</span></a>
                                </div>
                                <h3 class="post-title">
                                    <a href="single.html">Konsultan Bisnis</a>
                                </h3>
                                <div class="entry-meta meta-1 font-small color-grey mt-15 mb-15">
                                    <span class="post-by">By <a href="author.html">Syafira Razak</a></span>
                                </div>
                                <div class="post-excerpt mb-25">
                                    <p><?= lorem(20) ?>...</p>
                                </div>
                            </div>
                        </article>
                        <article class="col-lg-6 mb-50 animate-conner">
                            <div class="post-thumb d-flex mb-30 border-radius-5 img-hover-scale animate-conner-box">
                                <a href="single.html">
                                    <img src="assets/imgs/pnj-3.jpg" alt="">
                                </a>
                            </div>
                            <div class="post-content">
                                <div class="entry-meta meta-0 font-small mb-15">
                                    <a href="category.html"><span class="post-cat background2 color-white">Jasa</span></a>
                                </div>
                                <h3 class="post-title">
                                    <a href="single.html">Jasa Pindah Rumah</a>
                                </h3>
                                <div class="entry-meta meta-1 font-small color-grey mt-15 mb-15">
                                    <span class="post-by">By <a href="author.html">Ernest Prakasa</a></span>
                                </div>
                                <div class="post-excerpt mb-25">
                                    <p><?= lorem(20) ?>...</p>
                                </div>
                            </div>
                        </article>
                        <article class="col-lg-6 mb-50 animate-conner">
                            <div class="post-thumb d-flex mb-30 border-radius-5 img-hover-scale animate-conner-box">
                                <a href="single.html">
                                    <img src="assets/imgs/pnj-4.jpg" alt="">
                                </a>
                            </div>
                            <div class="post-content">
                                <div class="entry-meta meta-0 font-small mb-15">
                                    <a href="category.html"><span class="post-cat background2 color-white">Jasa</span></a>
                                </div>
                                <h3 class="post-title">
                                    <a href="single.html">Software House</a>
                                </h3>
                                <div class="entry-meta meta-1 font-small color-grey mt-15 mb-15">
                                    <span class="post-by">By <a href="author.html">Soleh Solehun</a></span>
                                </div>
                                <div class="post-excerpt mb-25">
                                    <p><?= lorem(20) ?>...</p>
                                </div>
                            </div>
                        </article>
                    </div>
                    <!--pagination-->
                    <div class="pagination-area pt-30 text-center bt-1 border-color-1">
                        <div class="container">
                            <div class="row">
                                <div class="col-xl-12 atext-center">
                                    <a href="" class="btn btn-dark text-center">View All</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-12">
                    <div class="widget-area">
                        <!--taber-->
                        <div class="sidebar-widget widget-taber mb-30">
                            <div class="widget-taber-content background-white pt-30 pb-30 pl-30 pr-30 border-radius-5">
                                <nav class="tab-nav float-none mb-20">
                                    <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                        <a class="nav-item nav-link active" id="nav-popular-tab" data-toggle="tab" href="#nav-popular" role="tab" aria-controls="nav-popular" aria-selected="true">Berita Terbaru</a>
                                    </div>
                                </nav>
                                <div class="tab-content">
                                    <!--Tab Popular-->
                                    <div class="tab-pane fade show active" id="nav-popular" role="tabpanel" aria-labelledby="nav-popular-tab">
                                        <div class="row">
                                            <div class="col-md-12 mb-20">  
                                                <div class="post-thumb position-relative thumb-overlay mb-15">
                                                    <div class="img-hover-slide border-radius-5 position-relative" style="background-image: url('assets/imgs/news-1.jpg')">
                                                        <a class="img-link" href="single.html"></a>
                                                    </div>
                                                </div>
                                                <div class="post-content">
                                                    <h4 class="post-title">
                                                        <a href="single.html">Vaksin Virus Covid-19 Sudah Ditemukan</a>
                                                    </h4>
                                                    <div class="entry-meta meta-1 font-small color-grey mt-15 mb-15">
                                                        <span class="post-on"><i class="ti-marker-alt"></i>25 April 2020</span>
                                                        <span class="time-reading"><i class="ti-timer"></i>10 mins read</span>
                                                        <span class="hit-count"><i class="ti-bolt"></i> 159k Views</span>
                                                    </div>
                                                    <p class="font-medium">These people envy me for having a lifestyle they don’t have, but the truth is, sometimes I envy their lifestyle instead. Struggling to sell one multi-million dollar home currently.</p>
                                                    <a class="readmore-btn font-small text-uppercase font-weight-ultra" href="single.html">Read More<i class="ti-arrow-right ml-5"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="post-block-list post-module-1">
                                            <ul class="list-post">
                                                <li class="mb-30">
                                                    <div class="d-flex">
                                                        <div class="post-thumb d-flex mr-15 border-radius-5 img-hover-scale">
                                                            <a href="single.html">
                                                                <img src="assets/imgs/news-2.jpg" alt="">
                                                            </a>
                                                        </div>
                                                        <div class="post-content media-body">
                                                            <div class="entry-meta meta-0 mb-10">
                                                                <a href="category.html"><span class="post-in background5 color-white font-small">World</span></a>
                                                            </div>
                                                            <h6 class="post-title mb-10 text-limit-2-row">Inilah Visi Misi Dr. Ary Zulfikar Calon Ketua IKA Universitas Padjadjaran</h6>
                                                            <div class="entry-meta meta-1 font-x-small color-grey">
                                                                <span class="post-on">25 April</span>
                                                                <span class="hit-count has-dot">26k Views</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="mb-30">
                                                    <div class="d-flex">
                                                        <div class="post-thumb d-flex mr-15 border-radius-5 img-hover-scale">
                                                            <a href="single.html">
                                                                <img src="assets/imgs/news-3.jpg" alt="">
                                                            </a>
                                                        </div>
                                                        <div class="post-content media-body">
                                                            <div class="entry-meta meta-0 mb-10">
                                                                <a href="category.html"><span class="post-in background7 color-white font-small">Films</span></a>
                                                            </div>
                                                            <h6 class="post-title mb-10 text-limit-2-row">Aji Mumpung Jualan Surat Bebas Corona Palsu di Tengah Pandemi</h6>
                                                            <div class="entry-meta meta-1 font-x-small color-grey mt-10">
                                                                <span class="post-on">25 April</span>
                                                                <span class="hit-count has-dot">37k Views</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="mb-30">
                                                    <div class="d-flex">
                                                        <div class="post-thumb d-flex mr-15 border-radius-5 img-hover-scale">
                                                            <a href="single.html">
                                                                <img src="assets/imgs/news-4.jpg" alt="">
                                                            </a>
                                                        </div>
                                                        <div class="post-content media-body">
                                                            <div class="entry-meta meta-0 mb-10">
                                                                <a href="category.html"><span class="post-in background2 color-white font-small">Travel</span></a>
                                                            </div>
                                                            <h6 class="post-title mb-10 text-limit-2-row">Bantuan Kasur Lipat untuk Ruang Perawatan Tambahan</h6>
                                                            <div class="entry-meta meta-1 font-x-small color-grey mt-10">
                                                                <span class="post-on">25 April</span>
                                                                <span class="hit-count has-dot">54k Views</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="d-flex">
                                                        <div class="post-thumb d-flex mr-15 border-radius-5 img-hover-scale">
                                                            <a href="single.html">
                                                                <img src="assets/imgs/news-5.jpg" alt="">
                                                            </a>
                                                        </div>
                                                        <div class="post-content media-body">
                                                            <div class="entry-meta meta-0 mb-10">
                                                                <a href="category.html"><span class="post-in background3 color-white font-small">Beauty</span></a>
                                                            </div>
                                                            <h6 class="post-title mb-10 text-limit-2-row">Bertambah 11, Positif Corona di Bengkulu Jadi 53 Kasus</h6>
                                                            <div class="entry-meta meta-1 font-x-small color-grey mt-10">
                                                                <span class="post-on">25 April</span>
                                                                <span class="hit-count has-dot">126k Views</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Ads -->
                        <div class="sidebar-widget widget-ads mb-30 text-center">
                            <a href="https://vimeo.com/333834999" class="play-video" data-animate="zoomIn" data-duration="1.5s" data-delay="0.1s">
                                <img class="d-inline-block" src="http://via.placeholder.com/432x200" alt="">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Recent Articles End -->
<?php
    include "footer.php";
?>