<!DOCTYPE html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Alumni ID</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="manifest" href="site.webmanifest">
    <link rel="shortcut icon" type="image/x-icon" href="assets/imgs/alumni-id.png">
    <!-- UltraNews CSS  -->
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/css/widgets.css">
    <link rel="stylesheet" href="assets/css/color.css">
    <link rel="stylesheet" href="assets/css/responsive.css">
    <link rel="stylesheet" href="assets/css/custome.css">
    <link rel="stylesheet" href="assets/plugin/fontawesome/css/all.min.css">
</head>

<body>
    <div class="scroll-progress primary-bg"></div>
    <!-- Preloader Start -->
    <div id="preloader-active">
        <div class="preloader d-flex align-items-center justify-content-center">
            <div class="preloader-inner position-relative">
                <div class="text-center">
                    <img src="assets/imgs/animate-load.gif">
                    <p>Loading...</p>
                </div>
            </div>
        </div>
    </div>
    <div class="main-wrap">
        <!--Offcanvas sidebar-->
        <aside id="sidebar-wrapper" class="custom-scrollbar offcanvas-sidebar">
        </aside>
        <!-- Main Wrap Start -->
        <header class="main-header header-style-1">
            <div class="top-bar pt-10 pb-10 background-white d-none d-md-block">
                <div class="container">
                    <div class="row">
                        <div class="col-6">
                            <div id="datetime" class="d-inline-block">
                                <ul>
                                    <li><span class="font-small"><i class="ti-calendar mr-5"></i><?= DATE('D ,d M Y') ?></span></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-6 text-right">
                            <ul class="header-social-network d-inline-block list-inline">
                                <li class="list-inline-item"><a class="social-icon facebook-icon text-xs-center" target="_blank" href="#"><i class="ti-facebook"></i></a></li>
                                <li class="list-inline-item"><a class="social-icon twitter-icon text-xs-center" target="_blank" href="#"><i class="ti-twitter-alt"></i></a></li>
                                <li class="list-inline-item"><a class="social-icon instagram-icon text-xs-center" target="_blank" href="#"><i class="ti-instagram"></i></a></li>
                            </ul>
                            <div class="vline-space d-inline-block"></div>
                            <div class="user-account d-inline-block font-small">
                                <a class="dropdown-toggle" href="#" role="button" id="userMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="ti-user"></i>
                                    <span>Akun</span>
                                </a>
                                <div id="userMenuDropdow" class="dropdown-menu dropdown-menu-right" aria-labelledby="userMenu">
                                    <a class="dropdown-item" href="#">Masuk</a>
                                    <a class="dropdown-item" href="#"><i class="ti-settings"></i>Daftar</a>
                                    <!-- <div class="dropdown-divider"></div> -->
                                    <!-- <a class="dropdown-item" href="login.html"><i class="ti-share"></i>Logout</a> -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="header-bottom header-sticky background-white text-center">
                <div class="mobile_menu d-lg-none d-block"></div>
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="off-canvas-toggle-cover">
                                <img class="logo-img d-inline" src="assets/imgs/alumni-id.png" alt="" width="70px">
                            </div>
                            <!-- <div class="logo-tablet d-md-inline d-lg-none d-none">
                                <a href="index.html">
                                    <img class="logo-img d-inline" src="assets/imgs/alumni-id.png" alt="">
                                </a>
                            </div>
                            <div class="logo-mobile d-inline d-md-none">
                                <a href="index.html">
                                    <img class="logo-img d-inline" src="assets/imgs/alumni-id.png" alt="">
                                </a>
                            </div> -->
                            <!-- Main-menu -->
                            <div class="main-nav text-center d-none d-lg-block">
                                <nav>
                                    <ul id="navigation" class="main-menu">
                                        <li><a href="home.php">Home</a></li>
                                        <li><a href="home.php">Berita</a></li>
                                        <li><a href="biography.php">Biografi</a></li>
                                        <li><a href="constitution.php">Undang - Undang</a></li>
                                        <li><a href="journal.php">Journal</a></li>
                                        <li><a href="profession.php">Profesi & Jasa Alumni</a></li>
                                        <li><a href="umkm.php">UMKM</a></li>
                                        <li><a href="info.php">Info</a></li>
                                        <li><a href="package.php">Paket Berlangganan</a></li>
                                    </ul>
                                </nav>
                            </div>
                            <!-- Search -->
                            <div class="search-button">
                                <button class="search-icon"><i class="ti-search"></i></button>
                                <span class="search-close float-right font-small"><i class="ti-close mr-5"></i>CLOSE</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
<?php
    if (!isset($bgColor)) {
        $bgColor = "";
    }
?>
<main class="position-relative pt-40 background12 <?= $bgColor; ?>">
    <!--Search Form-->
    <div class="main-search-form transition-02s">
        <div class="container">
            <div class="pt-10 pb-50 main-search-form-cover">
                <div class="row mb-20">
                    <div class="col-12">
                        <form action="#" method="get" class="search-form position-relative">
                            <div class="search-form-icon"><i class="ti-search"></i></div>
                            <label>
                                <input type="text" class="search_field" placeholder="Enter keywords for search..." value="" name="s">
                            </label>
                            <div class="search-switch">
                                <ul class="list-inline">
                                    <li class="list-inline-item"><a href="#" class="active">Cari</a></li>
                                </ul>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Recent Posts Start -->
<?php
// ffb602


    function lorem($limit, $add = "") {
    
        $loremText = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";

          if (str_word_count($loremText, 0) > $limit) {
              $words = str_word_count($loremText, 2);
              $pos = array_keys($words);
              $loremText = substr($loremText, 0, $pos[$limit]) . $add;
          }

      return $loremText;
    }
?>