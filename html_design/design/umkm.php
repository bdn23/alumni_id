<?php
    include "header.php";
?>

<main class="position-relative">
    <!--archive header-->
    <div class="archive-header text-center">
        <div class="container">
            <h2><span class="color2">UMKM</span></h2>
            <div class="breadcrumb">
                <a href="index.html" rel="nofollow">Home</a>
                <span></span>
                UMKM
            </div>
            <div class="bt-1 border-color-1 mt-30 mb-20"></div>
        </div>
    </div>
    <!--main content-->
</main>

<div class="pt-50 pb-0 background-white">
    <div class="main_content sidebar_right mt-5 pb-50">
        <div class="container">

            <div class="row">
                <div class="col-lg-3 col-md-12 col-sm-12 primary-sidebar sticky-sidebar">
                    <div class="sidebar-widget widget-about mb-10 pt-30 pr-20 pb-30 pl-20 background12 border-radius-5 overflow-auto">
                        <!--Categories-->
                        <div class="sidebar-widget widget_categories mb-50">
                            <div class="widget-header position-relative mb-20">
                                <h5 class="widget-title mt-5">Kategori</h5>
                            </div>
                            <div class="post-block-list post-module-1 post-module-5">
                                <ul>
                                    <li class="cat-item cat-item-2"><a href="category.html">Makanan Ringan</a> (3)</li>
                                    <li class="cat-item cat-item-3"><a href="category.html">Makanan Berat</a> (6)</li>
                                    <li class="cat-item cat-item-4"><a href="category.html">Makanan Sedang</a> (4)</li>
                                    <li class="cat-item cat-item-5"><a href="category.html">Elektronik</a> (3)</li>
                                    <li class="cat-item cat-item-6"><a href="category.html">Kesenian</a> (6)</li>
                                    <li class="cat-item cat-item-7"><a href="category.html">Pakaian</a> (2)</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-9 col-md-12 col-sm-12">
                    <!--loop-list-->

                    <div class="row mt-20 mb-50 author-bio ">
                        <div class="col-md-6">
                            <label>Nama Produk</label>
                            <input type="text" class="form-control" name="">
                        </div>
                        <div class="col-md-3">
                            <label>Kota</label>
                            <input type="text" class="form-control" name="">
                        </div>
                        <div class="col-md-3">
                            <button class="BSbtn BSbtn-lg BSbtn-outline-info mt-30"><i class="fas fa-search"></i> Search</button>
                        </div>
                    </div>
                    <div class="loop-grid row">

                        <?php 
                            for ($i=0; $i < 3; $i++) { 
                        ?>
                        <article class="col-lg-4 mb-40 animate-conner">
                            <div class="post-thumb d-flex mb-30 border-radius-5 img-hover-scale animate-conner-box">
                                <a href="single.html">
                                    <img src="assets/imgs/umkm-1.jpg" alt="">
                                </a>
                            </div>
                            <div class="post-content">
                                <div class="entry-meta meta-0 font-small mb-15">
                                    <a href="category.html"><span class="post-cat bg-warning color-white">Makanan Ringan</span></a>
                                </div>
                                <h3 class="post-title">
                                    <a href="single.html">Kripik Tempe Makioh</a>
                                </h3>
                                <div class="entry-meta meta-1 font-small color-grey mt-15 mb-15">
                                    <span class="post-by">By <a href="author.html">Melody Nurramdhani</a></span>
                                </div>
                                <div class="post-excerpt mb-25">
                                    <p>
                                        <?= lorem(8) ?>
                                    </p>
                                    <small>
                                        <table class="table table-sm borderless">
                                            <tr>
                                                <td><i class="fas fa-map-marker-alt"></i></td>
                                                <td>Jalan Panjang Lebar RT 01 / RW 02 <br> Ciawi, Bogor</td>
                                            </tr>
                                            <tr>
                                                <td><i class="fas fa-phone"></i></td>
                                                <td>0857 11200 377</td>
                                            </tr>
                                        </table>
                                    </small>
                                </div>
                            </div>
                        </article>
                        <article class="col-lg-4 mb-40 animate-conner">
                            <div class="post-thumb d-flex mb-30 border-radius-5 img-hover-scale animate-conner-box">
                                <a href="single.html">
                                    <img src="assets/imgs/umkm-2.jpg" alt="">
                                </a>
                            </div>
                            <div class="post-content">
                                <div class="entry-meta meta-0 font-small mb-15">
                                    <a href="category.html"><span class="post-cat bg-warning color-white">Makanan Ringan</span></a>
                                </div>
                                <h3 class="post-title">
                                    <a href="single.html">Dun Food</a>
                                </h3>
                                <div class="entry-meta meta-1 font-small color-grey mt-15 mb-15">
                                    <span class="post-by">By <a href="author.html">Syafira Razak</a></span>
                                </div>
                                <div class="post-excerpt mb-25">
                                    <p>
                                        <?= lorem(8) ?>
                                    </p>
                                    <small>
                                        <table class="table table-sm borderless">
                                            <tr>
                                                <td><i class="fas fa-map-marker-alt"></i></td>
                                                <td>Jalan Panjang Lebar RT 01 / RW 02 <br> Ciawi, Bogor</td>
                                            </tr>
                                            <tr>
                                                <td><i class="fas fa-phone"></i></td>
                                                <td>0857 11200 377</td>
                                            </tr>
                                        </table>
                                    </small>
                                </div>
                            </div>
                        </article>
                        <article class="col-lg-4 mb-40 animate-conner">
                            <div class="post-thumb d-flex mb-30 border-radius-5 img-hover-scale animate-conner-box">
                                <a href="single.html">
                                    <img src="assets/imgs/umkm-3.jpg" alt="">
                                </a>
                            </div>
                            <div class="post-content">
                                <div class="entry-meta meta-0 font-small mb-15">
                                    <a href="category.html"><span class="post-cat bg-warning color-white">Makanan Ringan</span></a>
                                </div>
                                <h3 class="post-title">
                                    <a href="single.html">Snack Ikan Wader</a>
                                </h3>
                                <div class="entry-meta meta-1 font-small color-grey mt-15 mb-15">
                                    <span class="post-by">By <a href="author.html">Ernest Prakasa</a></span>
                                </div>
                                <div class="post-excerpt mb-25">
                                    <p>
                                        <?= lorem(8) ?>
                                    </p>
                                    <small>
                                        <table class="table table-sm borderless">
                                            <tr>
                                                <td><i class="fas fa-map-marker-alt"></i></td>
                                                <td>Jalan Panjang Lebar RT 01 / RW 02 <br> Ciawi, Bogor</td>
                                            </tr>
                                            <tr>
                                                <td><i class="fas fa-phone"></i></td>
                                                <td>0857 11200 377</td>
                                            </tr>
                                        </table>
                                    </small>
                                </div>
                            </div>
                        </article>

                        
                        <?php 
                            }
                        ?>
                    </div>
                    <!--pagination-->
                    <div class="pagination-area pt-30 text-center bt-1 border-color-1">
                        <div class="container">
                            <div class="row">
                                <div class="col-xl-12">
                                    <div class="single-wrap d-flex justify-content-center">
                                        <nav aria-label="Page navigation example">
                                            <ul class="pagination justify-content-start">
                                                <li class="page-item"><a class="page-link" href="#"><i class="flaticon-left"></i> </a></li>
                                                <li class="page-item active"><a class="page-link" href="#">01</a></li>
                                                <li class="page-item"><a class="page-link" href="#">02</a></li>
                                                <li class="page-item"><a class="page-link" href="#">03</a></li>
                                                <li class="page-item"><a class="page-link" href="#"><i class="flaticon-right"></i> </a></li>
                                            </ul>
                                        </nav>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
    include "footer.php";
?>