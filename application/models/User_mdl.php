<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_mdl extends CI_Model {
	
	protected   $tbl_user 		 = "user",
				$tbl_group       = "groups",
				$tbl_user_group  = "user_group";


	function get_all_user($except_id=0){

		$keywords = (isset($_POST['search'])) ? $_POST['search']['value'] : "";
		$filterBy = (isset($_POST['filter'])) ? $_POST['filter']: "";

		$where    = "";
		if($keywords != ""){
			$fieldToSearch = array("username", "email", "display_name");
			$where = query_datatable_search($keywords, $fieldToSearch);
		}

		$mainQuery = "SELECT * from $this->tbl_user where id != 1 and id != ? $where";

		$queryData = query_datatable($mainQuery);

		$total = $this->db->query($mainQuery, $except_id)->num_rows();
		$data  = $this->db->query($queryData, $except_id)->result_array();

		$result['data']       = $data;
		$result['total_data'] = $total;

		return $result;	

	}

	function get_user_by_id($id){

		$sql = " SELECT mu.*, mg.id as group_id, mg.name as group_name from $this->tbl_user mu
						INNER JOIN $this->tbl_user_group mug on mu.id = mug.user_id
						INNER JOIN $this->tbl_group mg on mg.id = mug.group_id
						where mu.id = ?";

		$query = $this->db->query($sql, $id);

	    if ( $query->num_rows() > 0 )
	    {
	        return $query->row_array();
	    }

    }

	function get_all_groups(){

		$this->db->order_by("id");

		return $this->db->get($this->tbl_group)->result_array();	
	}


	function get_user_datatable(){

		$keywords = (isset($_POST['search'])) ? $_POST['search']['value'] : "";
		$filterBy = (isset($_POST['filter'])) ? $_POST['filter']: "";

		$where    = "";
		if($keywords != ""){
			$fieldToSearch = array("username" ,"email", "first_name");
			$where = query_datatable_search($keywords, $fieldToSearch);
		}

		$sql = "SELECT id, display_name, username, email, is_active
				FROM $this->tbl_user 
				WHERE ID NOT IN ? $where";

		$queryData = query_datatable_nolimit($sql);

		$total = $this->db->query($sql, array(array(1, $this->session->userdata('user_id'))))->num_rows();
		$data  = $this->db->query($queryData, array(array(1, $this->session->userdata('user_id'))))->result_array();

		$result['data']       = $data;
		$result['total_data'] = $total;

		return $result;	

	}

	function get_user_group($id){

		$sql = " SELECT g.* from groups g
					INNER JOIN user_group ug on g.id = ug.group_id
					where ug.user_id = ?";

		$query = $this->db->query($sql, $id);
		return $query->result_array();
	}

}

/* End of file Siswa_mdl.php */
/* Location: ./application/models/Siswa_mdl.php */