<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu_mdl extends CI_Model {
	
	protected   $tbl_menu = "dyn_menu";

	function menus()
	{
    	$this->db->select('*');
		$this->db->from($this->tbl_menu);
		$this->db->order_by('showorder');
		$this->db->where('id !=', 1);

		$query = $this->db->get();

		return $query->result_array();
	}


	function get_all_menu(){

		$keywords = (isset($_POST['search'])) ? $_POST['search']['value'] : "";
		$filterBy = (isset($_POST['filter'])) ? $_POST['filter']: "";

		$where    = "";
		if($keywords != ""){
			$fieldToSearch = array("title");
			$where = query_datatable_search($keywords, $fieldToSearch);
		}

		$mainQuery = "SELECT b.*, (SELECT title FROM $this->tbl_menu WHERE id = b.parent_id) as parent_name,
							(SELECT showorder FROM $this->tbl_menu WHERE id = b.parent_id) as parent_order
							FROM $this->tbl_menu b
							WHERE id != 1
							".$where."
							ORDER BY
								CASE
									WHEN parent_order IS NULL THEN 001
									ELSE parent_order
								END, b.showorder";

		$queryData = query_datatable($mainQuery);

		$total = $this->db->query($mainQuery)->num_rows();
		$data  = $this->db->query($queryData)->result_array();

		$result['data']       = $data;
		$result['total_data'] = $total;

		return $result;	

	}

	function xxx(){

		$where = "";

		if($keywords) {
			$q     = strtoupper($keywords);
			$where = " AND (
						UPPER(title) LIKE '%".$q."%'
						) ";
		}

		$mainQuery	= "SELECT b.*, (SELECT TITLE FROM $this->tbl_menu WHERE id = b.parent_id) as parent_name,
							(SELECT showorder FROM $this->tbl_menu WHERE id = b.parent_id) as parent_order
							FROM $this->tbl_menu b
							WHERE id != 1
							".$where."
							ORDER BY
								CASE
									WHEN parent_order IS NULL THEN 001
									ELSE parent_order
								END, b.showorder";

		$sql		= $mainQuery." limit ".$_POST['start'].",".$_POST['length'];

		$queryCount       = $this->db->query($mainQuery);
		$rowCount         = $queryCount->num_rows();
		$query            = $this->db->query($sql);
		
		$result['query']  = $query;
		$result['jmlRow'] = $rowCount;

		return $result;

	}

	function get_by_id($id){
		
    	$this->db->select('*');
		$this->db->from($this->tbl_menu); 
		$this->db->where('id', $id);
		$query = $this->db->get();

		return $query->row();
	}

	function get_parent(){

		$this->db->select("*");
		$this->db->from($this->tbl_menu);
		$this->db->where("link_type", "parent");
		$this->db->order_by("showorder");
		$query = $this->db->get();

		return $query->result_array();

	}

	function get_parent_single($id = NULL){

		$this->db->select("*");
		$this->db->from($this->tbl_menu);
		$this->db->where("link_type !=", "child");
		$this->db->where("id !=", 1);
		if($id != NULL){
			$this->db->where("id !=", $id);
		}
		$this->db->order_by("showorder");
		$query = $this->db->get();

		return $query->result_array();

	}

	function get_child($id, $idSelf = NULL){

		$this->db->select("*");
		$this->db->from($this->tbl_menu);
		$this->db->where("link_type", "child");
		$this->db->where("parent_id", $id);
		if($idSelf != NULL){
			$this->db->where("id !=", $idSelf);
		}
		$this->db->order_by("showorder");
		$query = $this->db->get();

		return $query->result_array();

	}

	function update_multi_menu($update_data){
		$upd = $this->db->update_batch($this->tbl_menu, $update_data, 'id');
		foreach ($update_data as $key => $value) {
			$id = $value["id"];
			//simtax_update_history($this->tbl_menu, "UPDATE", $id);
		}
		return TRUE;
	}

	

}

/* End of file Menu_mdl.php */
/* Location: ./application/models/Menu_mdl.php */