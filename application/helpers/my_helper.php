<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if ( ! function_exists('list_month')){

	function list_month($lang="id"){

		$monthArr = array("", "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");

		if($lang == "eng"){
			$monthArr = array("", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
		}

		return $monthArr;

	}
}

if ( ! function_exists('log_to_file')){

	function log_to_file($data){
		file_put_contents("Log_".date('Ymdhis').".txt", $data);
		return true;
	}
}

if ( ! function_exists('log_query')){

	function log_query(){
		$CI = get_instance();
		file_put_contents("Log_Query_".date('Ymdhis').".txt", $CI->db->last_query());
		return true;
	}
}

if ( ! function_exists('echo_pre')){

	function echo_pre($data){
		echo "<pre>";
		print_r($data);
		echo "</pre>";
	}
}

if ( ! function_exists('encrypt_url')){

	function encrypt_url($string) {
		$CI = get_instance();
		$encryption_key = $CI->config->item('encryption_key');
		$ciphering      = $CI->config->item('ciphering');
		$encryption_iv  = $CI->config->item('encryption_iv');
		  
		$encryption = openssl_encrypt($string, $ciphering, $encryption_key, 0, $encryption_iv);

		return $encryption;
	}
}

if ( ! function_exists('decrypt_url')){
	 
	function decrypt_url($encrypted) {

		$CI = get_instance();
		$encryption_key = $CI->config->item('encryption_key');
		$ciphering      = $CI->config->item('ciphering');
		$encryption_iv  = $CI->config->item('encryption_iv');
		  
		$decryption = openssl_decrypt($encrypted, $ciphering, $encryption_key, 0, $encryption_iv); 

		return $decryption;
	}

}

if ( ! function_exists('base64url_encode')){
	 
	function base64url_encode($data) {
	  return rtrim(strtr(base64_encode($data), '+/', '-_'), '=');
	}

}

if ( ! function_exists('base64url_decode')){
	 
	function base64url_decode($data) {
	  return base64_decode(str_pad(strtr($data, '-_', '+/'), strlen($data) % 4, '=', STR_PAD_RIGHT));
	}

}

if ( ! function_exists('dateFormat')){
	
	function dateFormat($dateTime, $format = "", $ts=true, $lang="eng"){

		if($ts == true){
			$timestamp  = $dateTime;
		}
		else{
			$timestamp  = strtotime($dateTime);
		}

		$date       = date("d", $timestamp);
		$dateTh     = date("jS", $timestamp);
		$time       = date("H:i", $timestamp);
		$day        = date("l", $timestamp);
		$dayShort   = date("D", $timestamp);
		$month      = date("F", $timestamp);
		$monthInt   = date("m", $timestamp);
		$shortMonth = date("M", $timestamp);
		$year       = date("Y", $timestamp);

		if($lang == "id"){
			$dayArr   = array("Minggu","Senin","Selasa","Rabu","Kamis","Jumat","Sabtu");
			$day      = $dayArr[date("w", $timestamp)];

			$monthArr = array("", "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
			$month    = $monthArr[date("n", $timestamp)];

			$shortMonthArr = array("", "Jan", "Feb", "Mar", "Apr", "Mei", "Jun", "Jul", "Agu", "Sep", "Okt", "Nov", "Des");
			$shortMonth    = $shortMonthArr[date("n", $timestamp)];
		}

		switch ($format) {
			case '1':
				$dateString = $date.", ".$month." ".$year;
			break;
			case '2':
				$dateString = $day.", ".$month." ".$year;
			break;
			case '3':
				$dateString = $date."-".$month."-".$year;
			break;
			case '4':
				$dateString = $date."/".$monthInt."/".$year;
			break;
			case '5':
				$dateString = $date."-".$monthInt."-".$year;
			break;
			case 'shortmonth':
				$dateString = $date." ".$shortMonth." ".$year;
			break;
			case 'datetime':
				$dateString = $date."/".$monthInt."/".$year." ".$time;
			break;
			case 'date':
				$dateString = $date."/".$monthInt."/".$year;
			break;
			case 'monthonly':
				$dateString = $month;
			break;
			case 'with_day':
				$dateString = $dayShort .", ". $dateTh ." ". $month ." ". $year ." / ". $time;
			break;
			case 'genesa':
				$dateString = $dateTh ." ". $month ." ". $year ." / ". $time;
			break;
			default:
				$dateString = $date." ".$monthInt." ".$year;
			break;
		}

		return $dateString;
	}
}

if ( ! function_exists('date_db')){
	
	function date_db($dateString, $separator="/"){

		$dateString = explode($separator, $dateString);
		$date       = $dateString[2].$separator.$dateString[1].$separator.$dateString[0];
		$timestamp  = strtotime($date);
		$dateString = date("Y-m-d", $timestamp);

		return $dateString;
	}
}

if ( ! function_exists('query_datatable')){
	
	function query_datatable($sql){

		$CI = get_instance();
		$start   = ($CI->input->post('start')) ? $CI->input->post('start') : 0 ;
		$length  = ($CI->input->post('length')) ? $CI->input->post('length') : 10 ;
		$orderBy = $CI->input->post('sortorder');

		$orderby = ($orderBy == "") ? " order by id desc " : " order by ".str_replace("sort_order_", "", $orderBy);

		$query = $sql." limit ".$start.",".$length;

		return $query;
	}
}

if ( ! function_exists('query_datatable_nolimit')){
	
	function query_datatable_nolimit($sql){

		$CI = get_instance();

		$start   = ($CI->input->post('start')) ? $CI->input->post('start') : 0 ;
		$length  = ($CI->input->post('length')) ? $CI->input->post('length') : 10 ;
		$orderBy = $CI->input->post('sortorder');

		$orderby = ($orderBy == "") ? " order by id desc " : " order by ".str_replace("sort_order_", "", $orderBy);

		$query = $sql;

		return $query;
	}
}

if ( ! function_exists('query_datatable_search')){
	
	function query_datatable_search($keywords, $data){

		$CI     = get_instance();
		$search = strtolower($keywords);
		$string = "";
		if(is_array($data)){
			for ($i=0; $i < count($data); $i++) {

				if($i==0){
					$string .= " AND ( ";
					$string .= $data[$i] . " like '%" . $CI->db->escape_like_str($search)."%' ESCAPE '!'";
				}
				else{
					$string .= " or ". $data[$i] . " like '%" . $CI->db->escape_like_str($search)."%' ESCAPE '!'";
				}
			}
			$string .= ")";

		}else{
			$string = " AND ". $data[$i] . " like '%" . $CI->db->escape_like_str($search)."%' ESCAPE '!'";
		}

		return $string;

	}
}

if ( ! function_exists('get_user_data')){
	
	function get_user_data($id, $field="display_name"){

		$CI = get_instance();
		$CI->load->model('user_mdl', 'user');

		$data = $CI->user->get_user_by_id($id);

		return $data[$field];

	}
}

if ( ! function_exists('get_group_data')){
	
	function get_group_data($id){

		$CI = get_instance();

		$data = $CI->crud->read_by_id("MASTER_GROUPS", $id);

		return $data;

	}
}


if ( ! function_exists('horizontal_loop_excel')){
	
	function horizontal_loop_excel($start, $lenght){
		$letter = $start;
		$step   = ($lenght) ? $lenght : 1000;
		for($i = 0; $i < $lenght; $i++) {
			$letters[] = $letter;
		    $letter++;
		}
		return $letters;
	}
}

if ( ! function_exists('generateRandomString')){
	
	function generateRandomString($length = 10) {
	    $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}
}

if ( ! function_exists('trim_string')){

	function trim_string($string, $type=""){

		$replace1 = str_replace(",", "", $string);
		$replace2 = str_replace(".", "", $replace1);
		$replace3 = preg_replace('/\s+/', '', $replace2);
		
		$type     = ($type != "") ? strtolower($type) : "";
		
		switch ($type) {
			case 'comma':
				$replace = str_replace(",", "", $string);
			break;
			case 'point':
				$replace = str_replace(".", "", $string);
			break;
			case 'space':
				$replace = preg_replace('/\s+/', '', $string);
			break;
			default:
				$replace = $replace3;
			break;
		}

		return $replace;

	}
}

if ( ! function_exists('delete_file')){
	
	function delete_file($path){

        $result['status']   = false;
        $result['messages'] = "";
        
        if (!file_exists(FCPATH.$path)){
            $result['messages'] = "File does not exist";
        }
        else{
            if(unlink($path)){
                $result['status']   = true;
            }
            else{
                $result['messages'] = "Failed to delete file";
            }
        }

		return $result;
    }
}


if ( ! function_exists('sendemail')){
	
	function sendemail($recipient, $subject, $body, $cc="", $attachment=""){

		$CI = get_instance();

		$config = array(
				"protocol"    => "smtp",
				"smtp_host"   => "mail.financetools.id",
				"smtp_user"   => "support@financetools.id",
				"smtp_pass"   => "H5zs@Y^MtR?}sGE7",
				"smtp_port"   => 465,
				"smtp_crypto" => "ssl",
				"charset"     => "UTF-8",
				"wordwrap"    => TRUE
          );

        $CI->load->library('email', $config);
		$CI->email->set_mailtype('html');
		$CI->email->set_newline("\r\n");
        $CI->email->from('support@financetools.id', "Finance Tools");
		$CI->email->to($recipient);
		$CI->email->subject($subject);
        $CI->email->message($body);

		if($cc != ""){
			$CI->email->cc($cc);
		}
		if($attachment != ""){
			$CI->email->attach($attachment);
		}
        if(base_url() == "https://financetools.id/apps/dev/"){
	        if ($CI->email->send()) {
	        	return true;
	        }
	        else{
	            return $CI->email->print_debugger();
	        }
        }
        else{
        	return true;
        }
    }
}