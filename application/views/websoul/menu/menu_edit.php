<div class="panel panel-bordered">
  <div class="panel-heading">
    <h3 class="panel-title"><?= $title ?></h3>
  </div>
  <div class="panel-body container-fluid pt-20">
    <div class="row">
      <div class="col-md-8">

        <?php if($this->session->flashdata('message') == true): ?>
        <div class="alert dark alert-danger alert-dismissible p-5" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
          <?= $this->session->flashdata('message') ?>
        </div>
      <?php endif; ?>
        <?= form_open('', array('class' => 'form-horizontal', 'autocomplete' => 'off')); ?>
          <div class="form-group row form-material">
            <label class="col-md-3 form-control-label">Name: </label>
            <div class="col-md-9">
              <input type="hidden" class="form-control" name="id" value="<?= $group['id']?>">
              <input type="text" class="form-control" name="name" placeholder="Name" autocomplete="off" value="<?= (set_value('name')) ? set_value('name') : $group['name']; ?>" required>
            </div>
          </div>
          <div class="form-group row form-material">
            <label class="col-md-3 form-control-label">Description: </label>
            <div class="col-md-9">
              <textarea name="description" class="form-control" cols="30" rows="4"><?= (set_value('description')) ? set_value('description') : $group['description']; ?></textarea>
            </div>
          </div>
          <div class="form-group row form-material">
            <div class="col-md-9 offset-md-3">
              <button type="submit" class="btn btn-primary">Submit </button>
              <button type="reset" class="btn btn-warning">Reset</button>
            </div>
          </div>
      <?= form_close(); ?>
      </div>
    </div>
  </div>
</div>

<script>

  $(document).ready(function() {


   $('.multi-select-methods').multiSelect();
    $('#buttonSelectAll').click(function() {
      $('.multi-select-methods').multiSelect('select_all');
      return false;
    });
    $('#buttonDeselectAll').click(function() {
      $('.multi-select-methods').multiSelect('deselect_all');
      return false;
    });
  });

</script>
<script src="<?= base_url('assets/websoul/') ?>global/vendor/multi-select/jquery.multi-select.js"></script>