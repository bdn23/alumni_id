<div class="panel panel-bordered">
	<div class="panel-heading">
		<h3 class="panel-title"><?= $title ?></h3>
 		<div class="panel-actions pt-10">
			<a href="<?= base_url('websoul/group/add') ?>" class="btn btn-primary mb-10 btn-sm"><i class="icon md-account-add" aria-hidden="true"></i> Add New</a>
        </div>
 	</div>
 	<div class="panel-body container-fluid pt-20">
 		<table id="table_data" class="table dataTable table-bordered table-striped table-responsive w-full hover" cellspacing="0">
 			<thead>
 				<tr>
 					<th>No.</th>
 					<th>Title</th>
 					<th>URL</th>
 					<th>Actions</th>
 					<th>Swaporder</th>
 				</tr>
	      </thead>
	  </table>
	</div>
</div>

<!-- Modal Form-->
<div class="modal fade modal-primary" id="modal-view" aria-hidden="true" aria-labelledby="modal-view" role="dialog" tabindex="-1">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title" id="modal-view-label"></h4>
      </div>
      <div class="modal-body">
        <div class="panel">
    		<form class="form-horizontal">
		        <div class="form-group row form-material row">
		            <label class="col-md-3 form-control-label">Name: </label>
		            <div class="col-md-9 pl-0">
		            	<label id="name" class="px-0 form-control-label"></label>
		            </div>
		        </div>
		        <div class="form-group row form-material row">
		            <label class="col-md-3 form-control-label">Description: </label>
		            <div class="col-md-9 pl-0">
		            	<label id="description" class="px-0 form-control-label"></label>
		            </div>
		        </div>
		    </form>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<!-- End Modal -->
<script>

	$(document).ready(function() {

		Pace.track(function(){
	       $('#table_data').DataTable({
	        "serverSide"    : true,
	        "processing"    : true,
	        "ajax"          : {
	                             "url"          : BASEURL + 'websoul/menu/api/load_menu',
	                             "type"         : "POST",
							      "data"    	: function ( d ) {
						        		d.<?= $this->security->get_csrf_token_name() ?> = readCookie(TOKEN_COOKIE_NAME);
							      	}
	                            },
	         "language"     : {
	                "emptyTable"  : "<span class='label label-danger'>Data Not Found!</span>",
					"infoEmpty"   : "Data Kosong",
					"processing"  : '<div class="loader vertical-align-middle loader-circle"></div>',
					"search"      : "_INPUT_"
	            },
	           "columns": [
	                { "data": "no", "width":"10px", "class":"text-center"},
	                { "data": "title", "width":"200px"},
	                { "data": "url", "width":"200px"},
	                { 
	                    "data": "title",
	                    "width":"80px",
	                    "class":"text-center",
	                    "render": function (data) {
	                     return '<a href="javascript:void(0)" class="action-edit" title="Edit ' + data + '" style="color:#ff6436"><i class="fa fa-edit" aria-hidden="true"></i></a> &nbsp;&nbsp;&nbsp; <a href="javascript:void(0)" class="action-delete" title="Hapus ' + data + '" style="color:#ff6436"><i class="fa fa-trash" aria-hidden="true"></i></a>';
	                    }
	                },
	                { "data": "showorder_icon", "width":"80px", "class":"text-center"}
	            ],
	            "scrollY"          : false, 
	            "scrollCollapse"   : true,
	            "scrollX"          : true,
	            "ordering"         : false,
	            "pageLength": 100,
	            drawCallback: function (settings) {
	                let api         = this.api();
	                let row_datas   = api.rows({ page: 'current' }).data();
	                let row_nodes   = api.rows({ page: 'current' }).nodes();
	                let last_parent = "";
	                row_datas.each(function (data, i) {
	                    group_parent = data.parent;
	                    if (last_parent != group_parent) {
	                        if(group_parent == 'XXX'){
	                            $(row_nodes).eq(i).before(
	                                '<tr class="group"><td align="center" colspan="6" style="color:#fff;background-color:#4caf50;font-weight:700;padding:10px 0;">Parent / Single Link</td></tr>'
	                            );
	                        }
	                        else{
	                            $(row_nodes).eq(i).before(
	                                '<tr class="group"><td align="center" colspan="6" style="color:#fff;background-color:#f44336;font-weight:700;padding:10px 0;">' +  group_parent  + '</td></tr>'
	                            );
	                        }
	                    }
	                    last_parent = group_parent;
	                });
	            }
	        });
	    });

	    table = $('#table_data').DataTable();


	    //View
	    $('#table_data tbody').on( 'dblclick', 'tr', function () {
			let data = table.row( this ).data();

			$("#name").html(data.name);
			$("#description").html(data.description);

        	$("#modal-view-label").html(data.name);
        	$("#modal-view").modal('show');
		} );

	    // Edit
	    $('#table_data tbody').on( 'click', 'a.edit-row', function () {
	        let data = table.row( $(this).parents('tr') ).data();
	        $(location).attr('href', BASEURL + 'websoul/group/edit/' + data.id);
	    });

	    // Delete
	    $('#table_data tbody').on( 'click', 'a.remove-row', function () {
	        let data      = table.row( $(this).parents('tr') ).data();
	        $("#modal-delete-label").html('Delete group : ' +  data.name);
	        $("#modal-delete").modal('show');
	    	let objDelete = {id:data.id, <?= $this->security->get_csrf_token_name() ?>:readCookie(TOKEN_COOKIE_NAME)}
			deleteData('websoul/group/api/delete/', objDelete);
	    });


	    <?php if($this->session->flashdata('message') == true): ?>
    		setTimeout(function(){
    			customNotif('Success!', '<?= $this->session->flashdata('message') ?>', 'success');
			}, 100);
      	<?php endif; ?>
	});

</script>