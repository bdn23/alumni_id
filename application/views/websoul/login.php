<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="bootstrap admin template">
  <meta name="author" content="">
  <title>Login | <?= $this->config->item('admin_title') ?></title>
  <link rel="apple-touch-icon" href="<?= base_url("assets/websoul/") ?>images/apple-touch-icon.png">
  <link rel="shortcut icon" href="<?= base_url("assets/websoul/") ?>images/favicon.ico">
  <!-- Stylesheets -->
  <link rel="stylesheet" href="<?= base_url("assets/websoul/") ?>global/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?= base_url("assets/websoul/") ?>global/css/bootstrap-extend.min.css">
  <link rel="stylesheet" href="<?= base_url("assets/websoul/") ?>css/site.min.css">
  <!-- Plugins -->
  <link rel="stylesheet" href="<?= base_url("assets/websoul/") ?>global/vendor/animsition/animsition.css">
  <link rel="stylesheet" href="<?= base_url("assets/websoul/") ?>global/vendor/asscrollable/asScrollable.css">
  <link rel="stylesheet" href="<?= base_url("assets/websoul/") ?>global/vendor/switchery/switchery.css">
  <link rel="stylesheet" href="<?= base_url("assets/websoul/") ?>global/vendor/intro-js/introjs.css">
  <link rel="stylesheet" href="<?= base_url("assets/websoul/") ?>global/vendor/slidepanel/slidePanel.css">
  <link rel="stylesheet" href="<?= base_url("assets/websoul/") ?>global/vendor/flag-icon-css/flag-icon.css">
  <link rel="stylesheet" href="<?= base_url("assets/websoul/") ?>global/vendor/waves/waves.css">
  <link rel="stylesheet" href="<?= base_url("assets/websoul/") ?>css/login.css">
  <!-- Fonts -->
  <link rel="stylesheet" href="<?= base_url("assets/websoul/") ?>global/fonts/material-design/material-design.min.css">
  <link rel="stylesheet" href="<?= base_url("assets/websoul/") ?>global/fonts/brand-icons/brand-icons.min.css">
  <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>
  <!--[if lt IE 9]>
    <script src="<?= base_url("assets/websoul/") ?>global/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->
  <!--[if lt IE 10]>
    <script src="<?= base_url("assets/websoul/") ?>global/vendor/media-match/media.match.min.js"></script>
    <script src="<?= base_url("assets/websoul/") ?>global/vendor/respond/respond.min.js"></script>
    <![endif]-->
  <!-- Scripts -->
  <script src="<?= base_url("assets/websoul/") ?>global/vendor/breakpoints/breakpoints.js"></script>
  <script>
  Breakpoints();
  </script>
</head>
<body class="animsition page-login layout-full page-dark">
  <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
  <!-- Page -->
  <div class="page vertical-align text-center" data-animsition-in="fade-in" data-animsition-out="fade-out">>
    <div class="page-content vertical-align-middle">
      <div class="brand">
        <img class="brand-img" src="<?= base_url("assets/websoul/") ?>/images/alumni-id.png" alt="...">
      </div>
      <p>Sign into your account</p>
      <?php if($this->session->flashdata()): ?>
      <div class="alert dark alert-danger alert-dismissible p-5" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
        <?= $this->session->flashdata('message') ?>
      </div>
    <?php endif; ?>
        <?= form_open('websoul/login', array('autocomplete' => 'off')); ?>
        <div class="form-group form-material floating" data-plugin="formMaterial">
          <input type="text" class="form-control empty" id="inputUsername" name="identity" autocomplete="off" required>
          <label class="floating-label" for="inputUsername">Username</label>
        </div>
        <div class="form-group form-material floating" data-plugin="formMaterial">
          <input type="password" class="form-control empty" id="inputPassword" name="password" autocomplete="off" required>
          <label class="floating-label" for="inputPassword">Password</label>
        </div>
        <div class="form-group clearfix">
          <div class="checkbox-custom checkbox-inline checkbox-primary float-left">
            <input type="checkbox" id="inputCheckbox" name="remember">
            <label for="inputCheckbox">Remember me</label>
          </div>
        </div>
        <button type="submit" class="btn btn-primary btn-block">Sign in</button>
      <?= form_close(); ?>
      <footer class="page-copyright page-copyright-inverse">
        <p><?= $this->config->item('admin_title') ?></p>
        <p>&copy; <?= date("Y") ?>. All RIGHT RESERVED.</p>
        <div class="social d-none">
          <a class="btn btn-icon btn-pure" href="javascript:void(0)">
            <i class="icon bd-twitter" aria-hidden="true"></i>
          </a>
          <a class="btn btn-icon btn-pure" href="javascript:void(0)">
            <i class="icon bd-facebook" aria-hidden="true"></i>
          </a>
          <a class="btn btn-icon btn-pure" href="javascript:void(0)">
            <i class="icon bd-google-plus" aria-hidden="true"></i>
          </a>
        </div>
      </footer>
    </div>
  </div>
  <!-- End Page -->
  <!-- Core  -->
  <script src="<?= base_url("assets/websoul/") ?>global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
  <script src="<?= base_url("assets/websoul/") ?>global/vendor/jquery/jquery.js"></script>
  <script src="<?= base_url("assets/websoul/") ?>global/vendor/tether/tether.js"></script>
  <script src="<?= base_url("assets/websoul/") ?>global/vendor/bootstrap/bootstrap.js"></script>
  <script src="<?= base_url("assets/websoul/") ?>global/vendor/animsition/animsition.js"></script>
  <script src="<?= base_url("assets/websoul/") ?>global/vendor/mousewheel/jquery.mousewheel.js"></script>
  <script src="<?= base_url("assets/websoul/") ?>global/vendor/asscrollbar/jquery-asScrollbar.js"></script>
  <script src="<?= base_url("assets/websoul/") ?>global/vendor/asscrollable/jquery-asScrollable.js"></script>
  <script src="<?= base_url("assets/websoul/") ?>global/vendor/ashoverscroll/jquery-asHoverScroll.js"></script>
  <script src="<?= base_url("assets/websoul/") ?>global/vendor/waves/waves.js"></script>
  <!-- Plugins -->
  <script src="<?= base_url("assets/websoul/") ?>global/vendor/switchery/switchery.min.js"></script>
  <script src="<?= base_url("assets/websoul/") ?>global/vendor/intro-js/intro.js"></script>
  <script src="<?= base_url("assets/websoul/") ?>global/vendor/screenfull/screenfull.js"></script>
  <script src="<?= base_url("assets/websoul/") ?>global/vendor/slidepanel/jquery-slidePanel.js"></script>
  <script src="<?= base_url("assets/websoul/") ?>global/vendor/jquery-placeholder/jquery.placeholder.js"></script>
  <!-- Scripts -->
  <script src="<?= base_url("assets/websoul/") ?>global/js/State.js"></script>
  <script src="<?= base_url("assets/websoul/") ?>global/js/Component.js"></script>
  <script src="<?= base_url("assets/websoul/") ?>global/js/Plugin.js"></script>
  <script src="<?= base_url("assets/websoul/") ?>global/js/Base.js"></script>
  <script src="<?= base_url("assets/websoul/") ?>global/js/Config.js"></script>
  <script src="<?= base_url("assets/websoul/") ?>js/Section/Menubar.js"></script>
  <script src="<?= base_url("assets/websoul/") ?>js/Section/GridMenu.js"></script>
  <script src="<?= base_url("assets/websoul/") ?>js/Section/Sidebar.js"></script>
  <script src="<?= base_url("assets/websoul/") ?>js/Section/PageAside.js"></script>
  <script src="<?= base_url("assets/websoul/") ?>js/Plugin/menu.js"></script>
  <script src="<?= base_url("assets/websoul/") ?>global/js/config/colors.js"></script>
  <script src="<?= base_url("assets/websoul/") ?>js/config/tour.js"></script>
  <script>
  Config.set('assets', '../../assets');
  </script>
  <!-- Page -->
  <script src="<?= base_url("assets/websoul/") ?>js/Site.js"></script>
  <script src="<?= base_url("assets/websoul/") ?>global/js/Plugin/asscrollable.js"></script>
  <script src="<?= base_url("assets/websoul/") ?>global/js/Plugin/slidepanel.js"></script>
  <script src="<?= base_url("assets/websoul/") ?>global/js/Plugin/switchery.js"></script>
  <script src="<?= base_url("assets/websoul/") ?>global/js/Plugin/jquery-placeholder.js"></script>
  <script src="<?= base_url("assets/websoul/") ?>global/js/Plugin/material.js"></script>
  <script>
  (function(document, window, $) {
    'use strict';
    var Site = window.Site;
    $(document).ready(function() {
      Site.run();
    });
   <?php if($this->session->flashdata()): ?>
   $("input:text:visible:first").focus();
   <?php endif; ?>
  })(document, window, jQuery);
  </script>
</body>
</html>