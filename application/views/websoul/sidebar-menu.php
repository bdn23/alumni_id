 <div class="site-menubar">
    <div class="site-menubar-body">
      <div>
        <div>
          <ul class="site-menu" data-plugin="menu">
            <li class="site-menu-category">General</li>
            <li class="site-menu-item">
              <a class="animsition-link" href="<?= base_url('websoul/') ?>">
                <i class="site-menu-icon md-view-dashboard" aria-hidden="true"></i>
                <span class="site-menu-title">Dashboard</span>
              </a>
            </li>
            <li class="site-menu-item has-sub">
              <a href="javascript:void(0)">
                <i class="site-menu-icon md-globe" aria-hidden="true"></i>
                <span class="site-menu-title">News</span>
                <span class="site-menu-arrow"></span>
              </a>
              <ul class="site-menu-sub">
                <li class="site-menu-item">
                  <a class="animsition-link" href="<?= base_url('websoul/users') ?>">
                    <span class="site-menu-title">List News</span>
                  </a>
                </li>
                <li class="site-menu-item">
                  <a class="animsition-link" href="<?= base_url('websoul/user/add') ?>">
                    <span class="site-menu-title">Add New</span>
                  </a>
                </li>
              </ul>
            </li>
            <li class="site-menu-category">Administrator</li>
            <li class="site-menu-item has-sub">
              <a href="javascript:void(0)">
                <i class="site-menu-icon md-account" aria-hidden="true"></i>
                <span class="site-menu-title">Users</span>
                <span class="site-menu-arrow"></span>
              </a>
              <ul class="site-menu-sub">
                <li class="site-menu-item">
                  <a class="animsition-link" href="<?= base_url('websoul/users') ?>">
                    <span class="site-menu-title">List User</span>
                  </a>
                </li>
                <li class="site-menu-item">
                  <a class="animsition-link" href="<?= base_url('websoul/user/add') ?>">
                    <span class="site-menu-title">Add New</span>
                  </a>
                </li>
              </ul>
            </li>
            <li class="site-menu-item has-sub">
              <a href="javascript:void(0)">
                <i class="site-menu-icon md-accounts" aria-hidden="true"></i>
                <span class="site-menu-title">Groups</span>
                <span class="site-menu-arrow"></span>
              </a>
              <ul class="site-menu-sub">
                <li class="site-menu-item">
                  <a class="animsition-link" href="<?= base_url('websoul/groups') ?>">
                    <span class="site-menu-title">List Group</span>
                  </a>
                </li>
                <li class="site-menu-item">
                  <a class="animsition-link" href="<?= base_url('websoul/group/add') ?>">
                    <span class="site-menu-title">Add New</span>
                  </a>
                </li>
              </ul>
            </li>
            <li class="site-menu-item has-sub">
              <a href="javascript:void(0)">
                <i class="site-menu-icon md-menu" aria-hidden="true"></i>
                <span class="site-menu-title">Menu Management</span>
                <span class="site-menu-arrow"></span>
              </a>
              <ul class="site-menu-sub">
                <li class="site-menu-item">
                  <a class="animsition-link" href="<?= base_url('websoul/menus') ?>">
                    <span class="site-menu-title">List Menu</span>
                  </a>
                </li>
                <li class="site-menu-item">
                  <a class="animsition-link" href="<?= base_url('websoul/menu/add') ?>">
                    <span class="site-menu-title">Add New</span>
                  </a>
                </li>
              </ul>
            </li>
            <li class="site-menu-category">Alumni ID</li>
            <li class="site-menu-item has-sub">
              <a href="javascript:void(0)">
                <i class="site-menu-icon md-graduation-cap" aria-hidden="true"></i>
                <span class="site-menu-title">Biografi</span>
                <span class="site-menu-arrow"></span>
              </a>
              <ul class="site-menu-sub">
                <li class="site-menu-item">
                  <a class="animsition-link" href="<?= base_url('websoul/biografi') ?>">
                    <span class="site-menu-title">List Biografi</span>
                  </a>
                </li>
                <li class="site-menu-item">
                  <a class="animsition-link" href="<?= base_url('websoul/biografi/add') ?>">
                    <span class="site-menu-title">Add New</span>
                  </a>
                </li>
              </ul>
            </li>
            <li class="site-menu-item has-sub">
              <a href="javascript:void(0)">
                <i class="site-menu-icon md-assignment" aria-hidden="true"></i>
                <span class="site-menu-title">Journal</span>
                <span class="site-menu-arrow"></span>
              </a>
              <ul class="site-menu-sub">
                <li class="site-menu-item">
                  <a class="animsition-link" href="<?= base_url('websoul/journal') ?>">
                    <span class="site-menu-title">List Journal</span>
                  </a>
                </li>
                <li class="site-menu-item">
                  <a class="animsition-link" href="<?= base_url('websoul/journal/add') ?>">
                    <span class="site-menu-title">Add New</span>
                  </a>
                </li>
              </ul>
            </li>
            <li class="site-menu-item has-sub">
              <a href="javascript:void(0)">
                <i class="site-menu-icon md-shield-check" aria-hidden="true"></i>
                <span class="site-menu-title">Undang-undang</span>
                <span class="site-menu-arrow"></span>
              </a>
              <ul class="site-menu-sub">
                <li class="site-menu-item">
                  <a class="animsition-link" href="<?= base_url('websoul/constitution') ?>">
                    <span class="site-menu-title">List Undang-undang</span>
                  </a>
                </li>
                <li class="site-menu-item">
                  <a class="animsition-link" href="<?= base_url('websoul/constitution/add') ?>">
                    <span class="site-menu-title">Add New</span>
                  </a>
                </li>
              </ul>
            </li>
            <li class="site-menu-item has-sub">
              <a href="javascript:void(0)">
                <i class="site-menu-icon md-shopping-cart" aria-hidden="true"></i>
                <span class="site-menu-title">UMKM</span>
                <span class="site-menu-arrow"></span>
              </a>
              <ul class="site-menu-sub">
                <li class="site-menu-item">
                  <a class="animsition-link" href="<?= base_url('websoul/umkm') ?>">
                    <span class="site-menu-title">List UMKM</span>
                  </a>
                </li>
                <li class="site-menu-item">
                  <a class="animsition-link" href="<?= base_url('websoul/umkm/add') ?>">
                    <span class="site-menu-title">Add New</span>
                  </a>
                </li>
              </ul>
            </li>
            <li class="site-menu-item has-sub">
              <a href="javascript:void(0)">
                <i class="site-menu-icon md-pin-account" aria-hidden="true"></i>
                <span class="site-menu-title">Profesi &amp; Jasa</span>
                <span class="site-menu-arrow"></span>
              </a>
              <ul class="site-menu-sub">
                <li class="site-menu-item">
                  <a class="animsition-link" href="<?= base_url('websoul/profesi-jasa') ?>">
                    <span class="site-menu-title">List Profesi &amp; Jasa</span>
                  </a>
                </li>
                <li class="site-menu-item">
                  <a class="animsition-link" href="<?= base_url('websoul/profesi-jasa/add') ?>">
                    <span class="site-menu-title">Add New</span>
                  </a>
                </li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </div>
    <div class="site-menubar-footer d-none">
      <a href="javascript: void(0);" class="fold-show" data-placement="top" data-toggle="tooltip"
      data-original-title="Settings">
        <span class="icon md-settings" aria-hidden="true"></span>
      </a>
      <a href="javascript: void(0);" data-placement="top" data-toggle="tooltip" data-original-title="Lock">
        <span class="icon md-eye-off" aria-hidden="true"></span>
      </a>
      <a href="javascript: void(0);" data-placement="top" data-toggle="tooltip" data-original-title="Logout">
        <span class="icon md-power" aria-hidden="true"></span>
      </a>
    </div>
  </div>