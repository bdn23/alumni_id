<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="bootstrap admin template">
  <meta name="author" content="">
  <title><?= (isset($title)) ? $title ." - ". $this->config->item('admin_title') : $this->config->item('admin_title') ?></title>
  <link rel="apple-touch-icon" href="<?= base_url('assets/websoul/') ?>images/apple-touch-icon.png">
  <link rel="shortcut icon" href="<?= base_url('assets/websoul/') ?>images/favicon.ico">
  <!-- Stylesheets -->
  <link rel="stylesheet" href="<?= base_url('assets/websoul/') ?>global/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?= base_url('assets/websoul/') ?>global/css/bootstrap-extend.min.css">
  <link rel="stylesheet" href="<?= base_url('assets/websoul/') ?>css/site.min.css">
  <!-- Plugins -->
  <link rel="stylesheet" href="<?= base_url('assets/websoul/') ?>global/vendor/animsition/animsition.css">
  <link rel="stylesheet" href="<?= base_url('assets/websoul/') ?>global/vendor/asscrollable/asScrollable.css">
  <!-- Fonts -->
  <link rel="stylesheet" href="<?= base_url('assets/websoul/') ?>global/fonts/material-design/material-design.min.css">
  <link rel="stylesheet" href="<?= base_url('assets/websoul/') ?>global/fonts/brand-icons/brand-icons.min.css">
  <!-- <link rel="stylesheet" href="<?= base_url('assets/websoul/') ?>global/fonts/font-awesome/font-awesome.css"> -->
  <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>
  <?php if(isset($module) && $module == "datatable"): ?>
  <!-- Datatables CSS -->
  <link href="<?= base_url('assets/'); ?>plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
  <link href="<?= base_url('assets/'); ?>plugins/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">
  <?php endif; ?>
  <link rel="stylesheet" href="<?= base_url('assets/websoul/') ?>global/vendor/multi-select/multi-select.css">
  <link rel="stylesheet" href="<?= base_url('assets/websoul/') ?>global/vendor/bootstrap-sweetalert/sweetalert.css">
  <link rel="stylesheet" href="<?= base_url('assets/websoul/') ?>global/vendor/toastr/toastr.css">
  <link rel="stylesheet" href="https://safety.genesadirgantara.com/assets/websoul/examples/css/advanced/toastr.css">
  <link rel="stylesheet" href="<?= base_url('assets/') ?>css/custom.css">
  <!--[if lt IE 9]>
    <script src="<?= base_url('assets/websoul/') ?>global/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->
  <!--[if lt IE 10]>
    <script src="<?= base_url('assets/websoul/') ?>global/vendor/media-match/media.match.min.js"></script>
    <script src="<?= base_url('assets/websoul/') ?>global/vendor/respond/respond.min.js"></script>
    <![endif]-->
  <!-- Scripts -->
  <script src="<?= base_url('assets/websoul/') ?>global/vendor/breakpoints/breakpoints.js"></script>
  <script src="<?= base_url('assets/websoul/') ?>global/vendor/jquery/jquery.js"></script>
  <script>
    Breakpoints();
    
    const BASEURL           = '<?= base_url() ?>';
    const TOKEN             = '<?= $this->security->get_csrf_hash() ?>';
    const TOKEN_COOKIE_NAME = '<?= $this->config->item('csrf_cookie_name') ?>';
    const imageLoading      = BASEURL + 'assets/img/loading.gif';
    const imageLoadingSmall = BASEURL + 'assets/img/loading-small.gif';
    
    $.ajaxSetup({
      data: { '<?= $this->security->get_csrf_token_name() ?>': TOKEN },
    });
  </script>
</head>
<body>
  <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
  <?php $this->load->view('websoul/header') ?>
  <?php $this->load->view('websoul/sidebar-menu') ?>
 
  <!-- Page -->
  <div class="page">
    <div class="page-content">
      <?php if(isset($breadcrumbs)): ?>
      <ol class="breadcrumb breadcrumb-arrow">
          <?php foreach ($breadcrumbs as $key => $value):?>
            <?= ($value['active'] == false) ? '<li class="breadcrumb-item"><a href="'.$value['link'].'">'.$value['name'].'</a></li>' : '<li class="breadcrumb-item active">'.$value['name'].'</li>'?>
          <?php endforeach; ?>
      </ol>
      <?php endif; ?>
      <?= $content ?>
    </div>
  </div>
  <!-- End Page -->
  <!-- Footer -->
  <footer class="site-footer">
    <div class="site-footer-legal">&copy; <?= date("Y") ?> <a href="http://themeforest.net/item/remark-responsive-bootstrap-admin-template/11989202">Remark Template</a>

    </div>
    <div class="site-footer-right d-none">
      Crafted with <i class="red-600 icon md-favorite"></i> by <a href="http://themeforest.net/user/amazingSurge">amazingSurge</a>
    </div>
  </footer>
<a class="d-none" id="customNotif" data-plugin="toastr"
      data-container-id="toast-top-right"
      data-position-class="toast-top-right"
      data-progress-bar="true"
      href="javascript:void(0)" role="button"> </a>

<?php if(isset($module) && $module == "datatable"): ?>
<!-- Modal Delete-->
<div class="modal fade modal-warning" id="modal-delete" aria-hidden="true" aria-labelledby="modal-delete" role="dialog" tabindex="-1">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title" id="modal-delete-label">Delete Record</h4>
      </div>
      <div class="modal-body">
        <p>Are you sure want to delete this data? </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-warning" id="button-delete">Yes</button>
      </div>
    </div>
  </div>
</div>
<!-- End Modal -->
<?php endif; ?>

  <!-- Core  -->
  <script src="<?= base_url('assets/websoul/') ?>global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
  <script src="<?= base_url('assets/websoul/') ?>global/vendor/tether/tether.js"></script>
  <script src="<?= base_url('assets/websoul/') ?>global/vendor/bootstrap/bootstrap.js"></script>
  <script src="<?= base_url('assets/websoul/') ?>global/vendor/animsition/animsition.js"></script>
  <script src="<?= base_url('assets/websoul/') ?>global/vendor/mousewheel/jquery.mousewheel.js"></script>
  <script src="<?= base_url('assets/websoul/') ?>global/vendor/asscrollbar/jquery-asScrollbar.js"></script>
  <script src="<?= base_url('assets/websoul/') ?>global/vendor/asscrollable/jquery-asScrollable.js"></script>
  <script src="<?= base_url('assets/websoul/') ?>global/vendor/ashoverscroll/jquery-asHoverScroll.js"></script>
  <script src="<?= base_url('assets/websoul/') ?>global/vendor/toastr/toastr.js"></script>

<?php if(isset($module) && $module == "datatable"): ?>
  <!-- Pace -->
  <script src="<?= base_url('assets/')?>plugins/pace/js/pace.min.js"></script>
  <!-- Datatables -->
  <script src="<?= base_url('assets/'); ?>plugins/datatables/jquery.dataTables.min.js"></script>
  <script src="<?= base_url('assets/'); ?>plugins/datatables-responsive/js/dataTables.responsive.js"></script>
  <script src="<?= base_url('assets/websoul/') ?>global/vendor/bootstrap-sweetalert/sweetalert.js"></script>
<?php endif; ?>
 
  <!-- Scripts -->
  <script src="<?= base_url('assets/websoul/') ?>global/js/State.js"></script>
  <script src="<?= base_url('assets/websoul/') ?>global/js/Component.js"></script>
  <script src="<?= base_url('assets/websoul/') ?>global/js/Plugin.js"></script>
  <script src="<?= base_url('assets/websoul/') ?>global/js/Base.js"></script>
  <script src="<?= base_url('assets/websoul/') ?>global/js/Config.js"></script>
  <script src="<?= base_url('assets/websoul/') ?>js/Section/Menubar.js"></script>
  <script src="<?= base_url('assets/websoul/') ?>js/Section/GridMenu.js"></script>
  <script src="<?= base_url('assets/websoul/') ?>js/Section/Sidebar.js"></script>
  <script src="<?= base_url('assets/websoul/') ?>js/Plugin/menu.js"></script>
  <script src="<?= base_url('assets/websoul/') ?>global/js/Plugin/toastr.js"></script>
  <script src="<?= base_url('assets/websoul/') ?>global/vendor/magnific-popup/jquery.magnific-popup.js"></script>
  <script>
  Config.set('assets', '<?= base_url('assets/websoul/') ?>');
  </script>
  <!-- Page -->
  <script src="<?= base_url('assets/websoul/') ?>js/Site.js"></script>
  <script src="<?= base_url('assets/websoul/') ?>global/js/Plugin/asscrollable.js"></script>
  <script src="<?= base_url('assets/') ?>js/custom.js"></script>
  <script>
  (function(document, window, $) {
    'use strict';
    var Site = window.Site;
    $(document).ready(function() {
      Site.run();
    });
  })(document, window, jQuery);
  </script>
</body>
</html>