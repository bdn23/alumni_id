<div class="panel panel-bordered">
  <div class="panel-heading">
    <h3 class="panel-title"><?= $title ?></h3>
  </div>
  <div class="panel-body container-fluid pt-20">
    <div class="row">
      <div class="col-md-8">

          <?php if($this->session->flashdata('success')): ?>
          <div class="alert dark alert-success alert-dismissible p-5" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
            <?= $this->session->flashdata('success') ?>
          </div>
        <?php endif; ?>
          <?php if($this->session->flashdata('errors')): ?>
          <div class="alert dark alert-danger alert-dismissible p-5" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
            <?= $this->session->flashdata('errors') ?>
          </div>
        <?php endif; ?>
        <?= form_open('', array('class' => 'form-horizontal', 'autocomplete' => 'off')); ?>
          <div class="form-group row form-material">
            <label class="col-md-3 form-control-label">Username: </label>
            <div class="col-md-9">
              <input type="hidden" class="form-control" name="id" value="<?= $user['id'] ?>">
              <input type="text" class="form-control" name="username" placeholder="Username" autocomplete="off" value="<?= $user['username']?>" readonly>
            </div>
          </div>
          <div class="form-group row form-material">
            <label class="col-md-3 form-control-label">Name: </label>
            <div class="col-md-9">
              <input type="text" class="form-control" name="name" placeholder="Name" autocomplete="off" value="<?= (set_value('name')) ? set_value('name') : $user['display_name']; ?>" required>
            </div>
          </div>
          <div class="form-group row form-material">
            <label class="col-md-3 form-control-label">Email: </label>
            <div class="col-md-9">
              <input type="email" class="form-control" name="email" placeholder="@alumni.id" autocomplete="off" value="<?= (set_value('email')) ? set_value('email') : $user['email']; ?>" required>
            </div>
          </div>
          <div class="form-group row form-material">
            <div class="col-md-9 offset-md-3">
              <button type="submit" class="btn btn-primary">Submit </button>
              <button type="reset" class="btn btn-warning">Reset</button>
            </div>
          </div>
      <?= form_close(); ?>
      </div>
    </div>
  </div>
</div>

<script>

  $(document).ready(function() {

  });

</script>
