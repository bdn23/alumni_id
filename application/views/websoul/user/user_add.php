<div class="panel panel-bordered">
  <div class="panel-heading">
    <h3 class="panel-title"><?= $title ?></h3>
  </div>
  <div class="panel-body container-fluid pt-20">
    <div class="row">
      <div class="col-md-8">

        <?php if($this->session->flashdata()): ?>
        <div class="alert dark alert-danger alert-dismissible p-5" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
          <?= $this->session->flashdata('message') ?>
        </div>
      <?php endif; ?>
        <?= form_open('', array('class' => 'form-horizontal', 'autocomplete' => 'off')); ?>
          <div class="form-group row form-material">
            <label class="col-md-3 form-control-label">Username: </label>
            <div class="col-md-9">
              <input type="text" class="form-control" name="username" placeholder="Username" autocomplete="off" value="<?=set_value('username'); ?>"  pattern="^\S+$" required>
            </div>
          </div>
          <div class="form-group row form-material">
            <label class="col-md-3 form-control-label">Fullname: </label>
            <div class="col-md-9">
              <input type="text" class="form-control" name="name" placeholder="Name" autocomplete="off" value="<?= set_value('name'); ?>" required>
            </div>
          </div>
          <div class="form-group row form-material">
            <label class="col-md-3 form-control-label">Email: </label>
            <div class="col-md-9">
              <input type="email" class="form-control" name="email" placeholder="@alumni.id" autocomplete="off" value="<?= set_value('email'); ?>" required>
            </div>
          </div>
          <div class="form-group row form-material">
            <label class="col-md-3 form-control-label">Assign Groups: </label>
            <div class="col-md-9">
              <select class="multi-select-methods form-control" multiple id="group-list" name="groups[]">
                <?php foreach ($groups as $key => $value):?>
                  <option value="<?= $value['id'] ?>" <?= set_select('groups', $value['id'], false); ?>><?= ucwords($value['name']) ?></option>
                <?php endforeach; ?>
              </select>
               <div class="d-block mb-30 mt-10">
                <button class="btn btn-success btn-sm" id="buttonSelectAll" type="button">select all</button>
                <button class="btn btn-danger btn-sm" id="buttonDeselectAll" type="button">deselect all</button>
              </div>
            </div>
          </div>
          <div class="form-group row form-material">
            <div class="col-md-9 offset-md-3">
              <button type="submit" class="btn btn-primary">Submit </button>
              <button type="reset" class="btn btn-warning">Reset</button>
            </div>
          </div>
      <?= form_close(); ?>
      </div>
    </div>
  </div>
</div>

<script>

  $(document).ready(function() {


   $('.multi-select-methods').multiSelect();
    $('#buttonSelectAll').click(function() {
      $('.multi-select-methods').multiSelect('select_all');
      return false;
    });
    $('#buttonDeselectAll').click(function() {
      $('.multi-select-methods').multiSelect('deselect_all');
      return false;
    });
  });

</script>
<script src="<?= base_url('assets/websoul/') ?>global/vendor/multi-select/jquery.multi-select.js"></script>