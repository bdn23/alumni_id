<div class="panel panel-bordered">
	<div class="panel-heading">
		<h3 class="panel-title"><?= $title ?></h3>
 		<div class="panel-actions pt-10">
			<a href="<?= base_url('websoul/user/add') ?>" class="btn btn-primary mb-10 btn-sm"><i class="icon md-account-add" aria-hidden="true"></i> Add New</a>
			<!-- <button href="javascript:void(0)" id="button-edit" class="btn btn-warning mb-10 btn-sm" disabled><i class="icon md-border-color" aria-hidden="true"></i> Edit </button>
			<button href="javascript:void(0)" id="button-delete" class="btn btn-danger mb-10 btn-sm" disabled><i class="icon icon md-delete" aria-hidden="true"></i> Delete </button> -->
        </div>
 	</div>
 	<div class="panel-body container-fluid pt-20">
 		<table id="table_data" class="table dataTable table-bordered table-striped table-responsive w-full hover" cellspacing="0">
 			<thead>
 				<tr>
 					<th>No.</th>
 					<th>Username</th>
 					<th>Full Name</th>
 					<th>Email</th>
 					<th>Actions</th>
 				</tr>
	      </thead>
	  </table>
	</div>
</div>

<!-- Modal Form-->
<div class="modal fade modal-primary" id="modal-view" aria-hidden="true" aria-labelledby="modal-view" role="dialog" tabindex="-1">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title" id="modal-view-label"></h4>
      </div>
      <div class="modal-body">
        <div class="panel">
    		<form class="form-horizontal">
    			<div class="form-group row form-material row">
		            <label class="col-md-3 form-control-label">Username: </label>
		            <div class="col-md-9 pl-0">
		            	<label id="username" class="px-0 form-control-label"></label>
		            </div>
		        </div>
		        <div class="form-group row form-material row">
		            <label class="col-md-3 form-control-label">Name: </label>
		            <div class="col-md-9 pl-0">
		            	<label id="name" class="px-0 form-control-label"></label>
		            </div>
		        </div>
		        <div class="form-group row form-material row">
		            <label class="col-md-3 form-control-label">Email: </label>
		            <div class="col-md-9 pl-0">
		            	<label id="email" class="px-0 form-control-label"></label>
		            </div>
		        </div>
		        <div class="form-group row form-material row">
		            <label class="col-md-3 form-control-label">Groups: </label>
		            <div class="col-md-9 pl-0">
		            	<label id="groups" class="px-0 form-control-label"></label>
		            </div>
		        </div>
		        <div class="form-group row form-material row">
		            <label class="col-md-3 form-control-label">Status: </label>
		            <div class="col-md-9 pl-0">
		            	<label id="status" class="px-0 form-control-label"></label>
		            </div>
		        </div>
		    </form>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<!-- End Modal -->
<script>

	$(document).ready(function() {
	    let url = BASEURL + 'websoul/user/api/load_user';
	    let ajaxData = {
	      "url"  : url,
	      "type" : "POST",
	      "data"    : function ( d ) {
        		d.<?= $this->security->get_csrf_token_name() ?> = readCookie(TOKEN_COOKIE_NAME);
	      	}
	      }
	    let jsonData = [
						    { "data": "no", "width":"10px", "class":"text-center"},
			                { "data": "username", "width":"100px", "class":"text-left"},
			                { "data": "fullname", "width":"200px", "class":"text-left"},
			                { "data": "email", "width":"100px"},
			                { 
			                  "data": "username",
			                  "width":"80px",
			                  "class":"text-center",
			                  "render": function (data) {
			                   return '<a href="javascript:void(0)" class="btn btn-md btn-icon btn-pure btn-primary on-default edit-row p-5" data-toggle="tooltip" data-original-title="Edit" title="Click to edit ' + data + '"><i class="icon md-border-color" aria-hidden="true"></i></a><a href="javascript:void(0)" class="btn btn-md btn-icon btn-pure btn-primary on-default remove-row p-5" data-toggle="tooltip" data-original-title="Remove"  title="Click to delete ' + data + '"><i class="icon md-delete" aria-hidden="true"></i></a><a href="javascript:void(0)" class="btn btn-md btn-icon btn-pure btn-primary on-default reset-pass p-5" data-toggle="tooltip" data-original-title="Remove"  title="Click to reset password ' + data + '"><i class="icon md-refresh" aria-hidden="true"></i></a>';
			                  }
			                }
		                ];

		data_table(ajaxData,jsonData);

	    table = $('#table_data').DataTable();


	    //View
	    $('#table_data tbody').on( 'dblclick', 'tr', function () {
			let data = table.row( this ).data();

			$("#username").html(data.username);
			$("#name").html(data.fullname);
			$("#email").html(data.email);
			$("#groups").html(data.group_name);
			$("#status").html(data.status);

        	$("#modal-view-label").html(data.fullname);
        	$("#modal-view").modal('show');
		} );

	    // Edit
	    $('#table_data tbody').on( 'click', 'a.edit-row', function () {
	        let data = table.row( $(this).parents('tr') ).data();
	        $(location).attr('href', BASEURL + 'websoul/user/edit/' + data.id);
	    });

	    // Delete
	    $('#table_data tbody').on( 'click', 'a.remove-row', function () {
	        let data      = table.row( $(this).parents('tr') ).data();
	        $("#modal-delete-label").html('Delete User : ' +  data.fullname);
	        $("#modal-delete").modal('show');
	    	let objDelete = {id:data.id, <?= $this->security->get_csrf_token_name() ?>:readCookie(TOKEN_COOKIE_NAME)}
			deleteData('websoul/user/api/delete/', objDelete);
	    });


	    // Reset pass
	    $('#table_data tbody').on( 'click', 'a.reset-pass', function () {

			let data = table.row( $(this).parents('tr') ).data();
	        swal({
	          title: "Are you sure?",
	          text:  "This user password will be reset!",
	          type: "warning",
	          showCancelButton: true,
	          confirmButtonClass: "btn-warning",
	          confirmButtonText: 'Yes, reset it!',
	          closeOnConfirm: false,
	        },
	        function() {
	            $.ajax({
	              url       : BASEURL + 'websoul/user/api/reset_password/',
	              type      : 'POST',
	              beforeSend  : function(){
	                              customLoading('show');
	                            },
	              dataType : 'json',
	              data:{id:data.id, <?= $this->security->get_csrf_token_name() ?>:readCookie(TOKEN_COOKIE_NAME)},
	              success : function(result){
	                id_reset = 0;
	                customLoading('hide');
	                if (result.status == true) {
	                  swal("Success!", result.messages, "success");
	                } else {
	                  swal("Failed!", result.messages, "error");
	                }
	              }
	            });
	        });
	    });

	    <?php if($this->session->flashdata('message') == true): ?>
    		setTimeout(function(){
    			customNotif('Success!', '<?= $this->session->flashdata('message') ?>', 'success');
			}, 100);
      	<?php endif; ?>
	});

</script>