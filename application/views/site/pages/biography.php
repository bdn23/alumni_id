
<main class="position-relative">
    <!--archive header-->
    <div class="archive-header text-center">
        <div class="container">
            <h2><span class="color2">Biografi</span></h2>
            <div class="breadcrumb">
                <a href="<?= base_url('home') ?>" rel="nofollow">Home</a>
                <span></span>
                Biografi
            </div>
            <div class="bt-1 border-color-1 mt-30 mb-20"></div>
            <div class="row">
                <div class="col-md-8"></div>
                <div class="col-md-4 mb-25">
                    <!-- <input type="text" class="form-control" name=""> -->
                  <input type="text" placeholder="Cari Biografi..." class="form-control bg-white">
                  <small class="float-right mt-1">*Enter untuk mencari</small>
                </div>
            </div>
        </div>
    </div>
    <!--main content-->
    <div class="main_content sidebar_right pb-50">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <!--loop-list-->
                    <div class="loop-metro post-module-1 row">
                    	<?php
                    		for ($i=1; $i < 7; $i++) { 
						?>

                        	<?php
                        		if ($i == 1) {
                        			$nameArtis = 'Dr. Ary Zulfikar';
                        		}elseif ($i == 2) {
                        			$nameArtis = 'Prof. Dr. Hj. Rina Indiastuti';
                        		}elseif ($i == 3) {
                        			$nameArtis = 'Hotman Paris';
                        		}elseif ($i == 4) {
                        			$nameArtis = 'Melody Nurramdhani';
                        		}elseif ($i == 5) {
                        			$nameArtis = 'Ernest Prakasa';
                        		}elseif ($i == 6) {
                        			$nameArtis = 'Chika Jessica';
                        		}

                        		$imageUrl = base_url('assets/site/').'imgs/ava-'.$i.'.jpg';
							?>
	                        <article class="col-lg-4 col-md-6 col-sm-12 mb-30">
	                            <div class="post-thumb position-relative">
	                                <div class="thumb-overlay img-hover-slide border-radius-5 position-relative" style="background-image: url(<?= $imageUrl ?>)">
	                                    <a class="img-link" href="biography-detail.php"></a>
	                                    <div class="post-content-overlay">
	                                        <h6 class="post-title">
	                                            <a class="color-white" href="biography-detail.php"><?= $nameArtis ?></a>
	                                        </h6>
		                                    <div class="entry-meta meta-1 font-small color-grey mt-10 pr-5 pl-5">
		                                        <span><?= lorem(12) ?></span>
		                                    </div>
	                                    </div>
	                                </div>
	                            </div>
	                        </article>
                    	<?php
                    		}
						?>

                    </div>
                    <!--pagination-->
                    <div class="pagination-area pt-30 text-center bt-1 border-color-1">
                        <div class="container">
                            <div class="row">
                                <div class="col-xl-12">
                                    <div class="single-wrap d-flex justify-content-center">
                                        <nav aria-label="Page navigation example">
                                            <ul class="pagination justify-content-start">
                                                <li class="page-item"><a class="page-link" href="#"><i class="flaticon-left"></i> </a></li>
                                                <li class="page-item active"><a class="page-link" href="#">01</a></li>
                                                <li class="page-item"><a class="page-link" href="#">02</a></li>
                                                <li class="page-item"><a class="page-link" href="#">03</a></li>
                                                <li class="page-item"><a class="page-link" href="#"><i class="flaticon-right"></i> </a></li>
                                            </ul>
                                        </nav>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>