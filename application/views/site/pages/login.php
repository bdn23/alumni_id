    <link rel="stylesheet" href="<?= base_url('assets/site/') ?>plugin/treeview/tree_view.css">
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>

    <div class="container pt-50 pb-50">
        <div class="row mb-30">
            <div class="col-md-4 offset-md-4">
                <div class="widget-taber-content background-white pt-30 pb-30 pl-30 pr-30 border-radius-5"> 
                    
                    <div class="widget-header position-relative mb-30">
                        <h5 class="widget-title mb-30 text-uppercase color1 font-weight-ultra">Login</h5>
                        <div class="letter-background">L</div>
                    </div>
                    <hr>
                    <form class="form-contact comment_form" action="#" id="commentForm">
                        <div class="row"> 
                            <div class="col-12">
                                <div class="form-group">
                                    <input class="form-control" name="email" id="email" type="email" placeholder="Email">
                                </div>
                            </div>                                  
                            <div class="col-12">
                                <div class="form-group">
                                    <input class="form-control" name="password" id="password" type="password" placeholder="Password">
                                </div>
                            </div>
                        </div>
                        <div class="form-group mt-2">
                            <button type="submit" class="BSbtn BSbtn-lg BSbtn-block BSbtn-success float-right">Login</button>
                        </div>
                        <div class="row pt-20 text-center"> 
                            <div class="col-12">
                                <p>Belum punya Akun? <a href="<?= base_url('sign-up') ?>">Daftar disini.</a></p>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script src="<?= base_url('assets/site/') ?>plugin/treeview/tree_view.js"></script>