<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home_ctl extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	
	public function index()
	{

		$data['title']          = "Home";

		$this->load->view('site/header', $data);
		$this->load->view('site/pages/home', $data);
		$this->load->view('site/footer');
	}


	public function error_404()
	{
		$this->load->view('error_404');
	}

	public function phpinfo()
	{
		phpinfo();
	}

	public function timeNow()
	{
		echo date('H:i');;
	}

	public function get_session()
	{
		echo_pre($this->session->userdata());
	}

}

/* End of file Home_ctl.php */
/* Location: ./application/controllers/Home_ctl.php */