<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Biography_ctl extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{

		$data['title']          = "Biography";

		$this->load->view('site/header', $data);
		$this->load->view('site/pages/biography', $data);
		$this->load->view('site/footer');
	}

}

/* End of file Biography_ctl.php */
/* Location: ./application/controllers/site/Biography_ctl.php */