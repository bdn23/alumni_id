<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class News_ctl extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{

		$data['title']          = "News";

		$this->load->view('site/header', $data);
		$this->load->view('site/pages/news', $data);
		$this->load->view('site/footer');
	}

}

/* End of file News_ctl.php */
/* Location: ./application/controllers/site/News_ctl.php */