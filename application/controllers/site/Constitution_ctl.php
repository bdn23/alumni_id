<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Constitution_ctl extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{

		$data['title'] = "Constitution";

		$this->load->view('site/header', $data);
		$this->load->view('site/pages/constitution', $data);
		$this->load->view('site/footer');
	}

}

/* End of file Constitution_ctl.php */
/* Location: ./application/controllers/site/Constitution_ctl.php */