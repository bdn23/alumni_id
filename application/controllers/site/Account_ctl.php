<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account_ctl extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		
	}

	public function login()
	{

		$data['title'] = "Sign In";

		$this->load->view('site/header', $data);
		$this->load->view('site/pages/login', $data);
		$this->load->view('site/footer');
	}

	public function register()
	{

		$data['title'] = "Sign Up";

		$this->load->view('site/header', $data);
		$this->load->view('site/pages/register', $data);
		$this->load->view('site/footer');
	}

}

/* End of file Account_ctl.php */
/* Location: ./application/controllers/site/Account_ctl.php */