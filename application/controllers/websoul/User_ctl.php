<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_ctl extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		if (!$this->ion_auth->logged_in())
		{
			redirect('websoul/login', 'refresh');
		}

		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

		$this->lang->load('auth');
		$this->load->model('User_mdl', 'user');
	}

	public function index()
	{

		$data['title']         = "List User";
		$data['module']        = "datatable";
		$data['template_page'] = "websoul/user/users";

		$breadcrumb[] = array( "name" => "Home", "link" => base_url('websoul/'), "active" => false );
		$breadcrumb[] = array( "name" => "Users", "active" => true );

		$data['breadcrumbs']    = $breadcrumb;

		$this->template->load('websoul/main', $data['template_page'], $data);
	}

	public function load_user(){

		$result['data']            = "";
		$result['draw']            = "";
		$result['recordsTotal']    = 0;
		$result['recordsFiltered'] = 0;

		$get_all = $this->user->get_all_user($this->session->userdata('user_id'));
		$data    = $get_all['data'];
		$total   = $get_all['total_data'];
		$start   = $this->input->post('start');
		$number  = $start+1;

		if($total > 0){

			foreach($data as $value) {

				$groupName = array();
				foreach ($this->user->get_user_group($value['id']) as $key => $group) {
					$groupName[] = ucwords($group['name']);
				}
				$groupName = implode(", ", $groupName);

				$row[] = array(
						'no'         => $number,
						'id'         => $value['id'],
						'username'   => $value['username'],
						'fullname'   => $value['display_name'],
						'email'      => $value['email'],
						'group_name' => $groupName,
						'status'     => ($value['active'] == 1) ? 'Active' : 'Not Active',
						);
				$number++;

			}

			$result['data']            = $row;
			$result['draw']            = ($this->input->post('draw')) ? $this->input->post('draw') : 0;
			$result['recordsTotal']    = $total;
			$result['recordsFiltered'] = $total;

		}

		echo json_encode($result);
	}

	public function add()
	{

		$data['title']         = "Add New User";
		$data['groups']        = $this->user->get_all_groups();
		$data['template_page'] = "websoul/user/user_add";

		$breadcrumb[] = array( "name" => "Home", "link" => base_url('websoul/'), "active" => false );
		$breadcrumb[] = array( "name" => "Users", "link" => base_url('websoul/users'), "active" => false );
		$breadcrumb[] = array( "name" => "Add New User", "active" => true );

		$data['breadcrumbs']    = $breadcrumb;

		if($_POST){

			$this->form_validation->set_rules('username', 'Username', 'alpha_numeric_spaces|trim|required|min_length[5]|max_length[15]');
			$this->form_validation->set_rules('name', 'Display Name', 'trim|required|min_length[5]');
			$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
			$this->form_validation->set_rules('groups[]', 'Groups', 'required');

			if ($this->form_validation->run() === TRUE){
				
				$identity = $this->input->post('username');
				$email    = strtolower($this->input->post('email'));
				$password = $this->config->item('default_password');

				$data = array(
					'display_name' => $this->input->post('name')
				);

				$insert = $this->ion_auth->register($identity, $password, $email, $data);

				if ($insert) {
					$groups = $this->input->post('groups');
					$this->ion_auth->add_to_group($groups, $insert);
					$this->session->set_flashdata('message', 'Successfully add new user');
					redirect('/websoul/users','refresh');
				}

			}else{
				$this->session->set_flashdata('message', validation_errors());
			}

		}

		$this->template->load('websoul/main', $data['template_page'], $data);
	}

	public function edit( int $id )
	{

		if($_POST){

			$this->form_validation->set_rules('name', 'Display Name', 'trim|required|min_length[5]');
			$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
			$this->form_validation->set_rules('groups[]', 'Groups', 'required');

			if ($this->form_validation->run() === TRUE){
				
				$id = $this->input->post('id');
				$data = array(
					'email'        => strtolower($this->input->post('email')),
					'display_name' => $this->input->post('name'),
					'active'       => ($this->input->post('status') == "active") ? 1 : 0
				);

				if ($this->crud->update("user", $data, $id) > 0) {

					$this->ion_auth->remove_from_group('', $id);
					$this->ion_auth->add_to_group($this->input->post('groups'), $id);
					$this->session->set_flashdata('message', 'Data successfully updated');

				}
				redirect('/websoul/users','refresh');

			}else{
				$this->session->set_flashdata('message', validation_errors());
			}

		}

		$user = $this->user->get_user_by_id($id);

		if(!$user){
			redirect('/websoul/users','refresh');
		}

		$data['title']         = "Edit User ".$user['display_name'];
		$data['user']          = $user;
		$data['groups']        = $this->user->get_all_groups();
		$data['template_page'] = "websoul/user/user_edit";

		$user_groups = array();
		foreach ($this->ion_auth->get_users_groups($id)->result_array() as $key => $value) {
			$user_groups[] = $value['id'];
		}
		$data['user_groups']   = $user_groups;

		$breadcrumb[] = array( "name" => "Home", "link" => base_url('websoul/'), "active" => false );
		$breadcrumb[] = array( "name" => "Users", "link" => base_url('websoul/users'), "active" => false );
		$breadcrumb[] = array( "name" => "Edit User", "active" => true );

		$data['breadcrumbs']    = $breadcrumb;		

		$this->template->load('websoul/main', $data['template_page'], $data);
	}


	public function delete(){

		$id = (int) $this->input->post('id');

		$result['status']   = false;
		$result['messages'] = "Failed to delete data";

		if($this->crud->delete("user", $id) > 0){
			$result['status']   = true;
			$result['messages'] = "Data successfully deleted";
		}

		echo json_encode($result);
	}

	public function reset_password()
	{
		$id = (int) $this->input->post('id');

		$result['status']   = false;
		$result['messages'] = "Failed to reset password";
			
		$get_identity = $this->user->get_user_by_id($id);
		$identity     = $get_identity['username'];
		$new_password = $this->config->item('default_password');

		if($this->ion_auth->reset_password($identity, $new_password)){
			$result['status']   = true;
			$result['messages'] = "Password successfully reseted";
		}

		echo json_encode($result);
	}



	public function profile()
	{

		if($_POST){

			$this->form_validation->set_rules('name', 'Display Name', 'trim|required|min_length[5]');
			$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');

			if ($this->form_validation->run() === TRUE){
				
				$id = $this->input->post('id');
				$data = array(
					'email'        => strtolower($this->input->post('email')),
					'display_name' => $this->input->post('name')
				);

				$this->crud->update("user", $data, $id);
				$this->session->set_flashdata('success', 'Profile successfully updated');

			}else{
				$this->session->set_flashdata('errors', validation_errors());
			}
			redirect('websoul/user/profile', 'refresh');
		}

		$user = $this->user->get_user_by_id($this->session->userdata('user_id'));

		$data['title']         = "Edit Profile";
		$data['user']          = $user;
		$data['template_page'] = "websoul/user/user_profile";

		$breadcrumb[] = array( "name" => "Home", "link" => base_url('websoul/'), "active" => false );
		$breadcrumb[] = array( "name" => "Edit Profile", "active" => true );

		$data['breadcrumbs']    = $breadcrumb;

		$this->template->load('websoul/main', $data['template_page'], $data);
	}


}

/* End of file User_ctl.php */
/* Location: ./application/controllers/websoul/User_ctl.php */