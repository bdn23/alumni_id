<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Group_ctl extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		if (!$this->ion_auth->logged_in())
		{
			redirect('websoul/login', 'refresh');
		}

		$this->lang->load('auth');
		$this->load->model('group_mdl', 'group');
	}

	public function index()
	{

		$data['title']         = "List Group";
		$data['module']        = "datatable";
		$data['template_page'] = "websoul/group/groups";

		$breadcrumb[] = array( "name" => "Home", "link" => base_url('websoul/'), "active" => false );
		$breadcrumb[] = array( "name" => "Groups", "active" => true );

		$data['breadcrumbs']    = $breadcrumb;

		$this->template->load('websoul/main', $data['template_page'], $data);
	}

	public function load_group(){

		$result['data']            = "";
		$result['draw']            = "";
		$result['recordsTotal']    = 0;
		$result['recordsFiltered'] = 0;

		$get_all = $this->group->get_all_group();
		$data    = $get_all['data'];
		$total   = $get_all['total_data'];
		$start   = $this->input->post('start');
		$number  = $start+1;

		if($total > 0){

			foreach($data as $value) {

				$row[] = array(
						'no'          => $number,
						'id'          => $value['id'],
						'name'        => $value['name'],
						'description' => $value['description']
						);
				$number++;

			}

			$result['data']            = $row;
			$result['draw']            = ($this->input->post('draw')) ? $this->input->post('draw') : 0;
			$result['recordsTotal']    = $total;
			$result['recordsFiltered'] = $total;

		}

		echo json_encode($result);
	}

	public function add()
	{

		$data['title']         = "Add New Group";
		$data['template_page'] = "websoul/group/group_add";

		$breadcrumb[] = array( "name" => "Home", "link" => base_url('websoul/'), "active" => false );
		$breadcrumb[] = array( "name" => "Groups", "link" => base_url('websoul/groups'), "active" => false );
		$breadcrumb[] = array( "name" => "Add New Group", "active" => true );

		$data['breadcrumbs']    = $breadcrumb;

		if($_POST){

			$this->form_validation->set_rules('name', 'Name', 'required|min_length[2]');

			if ($this->form_validation->run() === TRUE){
				
				$data = array(
					'name'        => $this->input->post('name'),
					'description' => $this->input->post('description')
				);


				if ($this->crud->create("groups", $data) > 0) {
					$this->session->set_flashdata('message', 'Successfully add new group');
					redirect('/websoul/groups','refresh');
				}

			}else{
				$this->session->set_flashdata('message', validation_errors());
			}

		}

		$this->template->load('websoul/main', $data['template_page'], $data);
	}

	public function edit( int $id )
	{

		if($_POST){

			$this->form_validation->set_rules('name', 'Name', 'required|min_length[2]');

			if ($this->form_validation->run() === TRUE){
				
				$id = $this->input->post('id');

				$data = array(
					'name'        => $this->input->post('name'),
					'description' => $this->input->post('description')
				);

				$this->crud->update("groups", $data, $id);
				$this->session->set_flashdata('message', 'Data successfully updated');
				redirect('/websoul/groups','refresh');

			}else{
				$this->session->set_flashdata('message', validation_errors());
			}

		}

		$group = $this->crud->read_by_id("groups", $id);

		if(!$group){
			redirect('/websoul/groups','refresh');
		}

		$data['title']         = "Edit Group ".$group['name'];
		$data['group']         = $group;
		$data['template_page'] = "websoul/group/group_edit";

		$breadcrumb[] = array( "name" => "Home", "link" => base_url('websoul/'), "active" => false );
		$breadcrumb[] = array( "name" => "Groups", "link" => base_url('websoul/groups'), "active" => false );
		$breadcrumb[] = array( "name" => "Edit Group", "active" => true );

		$data['breadcrumbs']    = $breadcrumb;		

		$this->template->load('websoul/main', $data['template_page'], $data);
	}


	public function delete(){

		$id = (int) $this->input->post('id');

		$result['status']   = false;
		$result['messages'] = "Failed to delete data";

		if($this->crud->delete("groups", $id) > 0){
			$result['status']   = true;
			$result['messages'] = "Data successfully deleted";
		}

		echo json_encode($result);
	}


}

/* End of file Group_ctl.php */
/* Location: ./application/controllers/websoul/Group_ctl.php */