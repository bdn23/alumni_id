<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}

	
	public function index()
	{
		if (!$this->ion_auth->logged_in())
		{
			redirect('websoul/login', 'refresh');
		}

		$data['title']          = "Dashboard";
		$data['module']         = "dashboard";
		$data['template_page']  = "websoul/pages/dashboard";

		$this->template->load('websoul/main', $data['template_page'], $data);
	}


	public function error_404()
	{
		$this->load->view('error_404');
	}

	public function phpinfo()
	{
		phpinfo();
	}

	public function timeNow()
	{
		echo date('H:i');;
	}

	public function get_session()
	{
		echo_pre($this->session->userdata());
	}

}

/* End of file Home.php */
/* Location: ./application/controllers/websoul/Home.php */