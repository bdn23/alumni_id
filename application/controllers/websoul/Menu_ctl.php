<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu_ctl extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		if (!$this->ion_auth->logged_in())
		{
			redirect('websoul/login', 'refresh');
		}

		$this->lang->load('auth');
		$this->load->model('menu_mdl', 'menu');
	}

	public function index()
	{

		$data['title']         = "Menu Management";
		$data['module']        = "datatable";
		$data['template_page'] = "websoul/menu/menus";

		$breadcrumb[] = array( "name" => "Home", "link" => base_url('websoul/'), "active" => false );
		$breadcrumb[] = array( "name" => "Menu Management", "active" => true );

		$data['breadcrumbs']    = $breadcrumb;

		$this->template->load('websoul/main', $data['template_page'], $data);
	}

	public function load_menu(){

		$result['data']            = "";
		$result['draw']            = "";
		$result['recordsTotal']    = 0;
		$result['recordsFiltered'] = 0;

		$get_all = $this->menu->get_all_menu();
		$data    = $get_all['data'];
		$total   = $get_all['total_data'];
		$start   = $this->input->post('start');
		$number  = $start+1;

		if($total > 0){

			$previousID = 0;
			foreach($data as $value) {

				$parent    = ($value['parent_name'] != NULL) ? $value['parent_name'] : "XXX";
				$showorder = "<a href='javascript:void(0)' class='showorder' data-productid='".$value['showorder']."' data-order='".$value['showorder']."' data-parent='". strtolower(str_replace(' ', '_', $parent)) ."' style='color:#ff6436'><i class='fa fa-retweet' aria-hidden='true'></i></a>";

				$row[] = array(
							'no'              => $number,
							'id'              => $value['id'],
							'parent'          => $parent,
							'parent_id'       => $value['parent_id'],
							'category'        => $value['link_type'],
							'title'           => $value['title'],
							'url'             => $value['url'],
							'showorder_after' => $previousID,
							'showorder'       => $value['showorder'],
							'showorder_icon'  => $showorder
						);
				$previousID = $value['id'];
				$number++;

			}

			$result['data']            = $row;
			$result['draw']            = ($this->input->post('draw')) ? $this->input->post('draw') : 0;
			$result['recordsTotal']    = $total;
			$result['recordsFiltered'] = $total;

		}

		echo json_encode($result);

	}

	public function add()
	{

		$data['title']         = "Add New Menu";
		$data['template_page'] = "websoul/menu/menu_add";

		$breadcrumb[] = array( "name" => "Home", "link" => base_url('websoul/'), "active" => false );
		$breadcrumb[] = array( "name" => "Menus", "link" => base_url('websoul/menus'), "active" => false );
		$breadcrumb[] = array( "name" => "Add New Menu", "active" => true );

		$data['breadcrumbs']    = $breadcrumb;

		if($_POST){

			$this->form_validation->set_rules('name', 'Name', 'required|min_length[2]');

			if ($this->form_validation->run() === TRUE){
				
				$data = array(
					'name'        => $this->input->post('name'),
					'description' => $this->input->post('description')
				);


				if ($this->crud->create("menus", $data) > 0) {
					$this->session->set_flashdata('message', 'Successfully add new menu');
					redirect('/websoul/menus','refresh');
				}

			}else{
				$this->session->set_flashdata('message', validation_errors());
			}

		}

		$this->template->load('websoul/main', $data['template_page'], $data);
	}

	public function edit( int $id )
	{

		if($_POST){

			$this->form_validation->set_rules('name', 'Name', 'required|min_length[2]');

			if ($this->form_validation->run() === TRUE){
				
				$id = $this->input->post('id');

				$data = array(
					'name'        => $this->input->post('name'),
					'description' => $this->input->post('description')
				);

				$this->crud->update("menus", $data, $id);
				$this->session->set_flashdata('message', 'Data successfully updated');
				redirect('/websoul/menus','refresh');

			}else{
				$this->session->set_flashdata('message', validation_errors());
			}

		}

		$menu = $this->crud->read_by_id("menus", $id);

		if(!$menu){
			redirect('/websoul/menus','refresh');
		}

		$data['title']         = "Edit Menu ".$menu['name'];
		$data['menu']         = $menu;
		$data['template_page'] = "websoul/menu/menu_edit";

		$breadcrumb[] = array( "name" => "Home", "link" => base_url('websoul/'), "active" => false );
		$breadcrumb[] = array( "name" => "Menus", "link" => base_url('websoul/menus'), "active" => false );
		$breadcrumb[] = array( "name" => "Edit Menu", "active" => true );

		$data['breadcrumbs']    = $breadcrumb;		

		$this->template->load('websoul/main', $data['template_page'], $data);
	}


	public function delete(){

		$id = (int) $this->input->post('id');

		$result['status']   = false;
		$result['messages'] = "Failed to delete data";

		if($this->crud->delete("menus", $id) > 0){
			$result['status']   = true;
			$result['messages'] = "Data successfully deleted";
		}

		echo json_encode($result);
	}


}

/* End of file Menu_ctl.php */
/* Location: ./application/controllers/websoul/Menu_ctl.php */