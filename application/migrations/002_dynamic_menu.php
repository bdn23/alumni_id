<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Dynamic_menu extends CI_Migration {

        public function up()
        {

                // Dynamic Menu
                $this->dbforge->add_field(array(
                        'id' => array(
                                'type' => 'INT',
                                'constraint' => 11,
                                'unsigned' => TRUE,
                                'auto_increment' => TRUE
                        ),
                        'title' => array(
                                'type' => 'VARCHAR',
                                'constraint' => 100,
                                'null' => TRUE,
                        ),
                        'url' => array(
                                'type' => 'VARCHAR',
                                'constraint' => 250,
                        ),
                        'uri' => array(
                                'type' => 'VARCHAR',
                                'constraint' => 250,
                        ),
                        'showorder' => array(
                                'type' => 'INT',
                                'constraint' => 11,
                        ),
                        'show_menu' => array(
                                'type' => 'INT',
                                'constraint' => 11,
                        ),
                        'parent_id' => array(
                                'type' => 'INT',
                                'constraint' => 11,
                        ),
                        'is_parent' => array(
                                'type' => 'INT',
                                'constraint' => 11,
                        ),
                        'link_type' => array(
                                'type' => 'VARCHAR',
                                'constraint' => 30,
                        ),
                        'created_by' => array(
                                'type' => 'VARCHAR',
                                'constraint' => 150,
                        ),
                        'created_date timestamp default current_timestamp', 
                        'updated_by' => array(
                                'type' => 'VARCHAR',
                                'constraint' => 150,
                        ),
                        'updated_date timestamp default current_timestamp on update current_timestamp', 
                ));

                $this->dbforge->add_key('id', TRUE);
                $this->dbforge->create_table('dyn_menu');
        }

        public function down()
        {
                $this->dbforge->drop_table('dyn_menu');
                $this->dbforge->drop_table('trx_login_attempts');
        }
}