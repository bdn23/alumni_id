<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'Home_ctl';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;


// Auth
$login_route                    = $this->config->item('login_hash_url').$this->config->item('batch_code')."xKz";
$route[$login_route]            = 'ion_auth/auth/login_post';
$route['login']                 = 'ion_auth/auth/login';
$route['logout']                = 'ion_auth/auth/logout';


/* Websoul */

$route['websoul']        = 'websoul/home';
$route['websoul/login']  = 'websoul/login';
$route['websoul/logout'] = 'ion_auth/auth/logout/1';

// Users
$route['websoul/users']            = 'websoul/User_ctl';
$route['websoul/user/add']         = 'websoul/User_ctl/add';
$route['websoul/user/profile']     = 'websoul/User_ctl/profile';
$route['websoul/user/edit/(:any)'] = 'websoul/User_ctl/edit/$1';
$route['websoul/user/api/(:any)']  = 'websoul/User_ctl/$1';

// Groups
$route['websoul/groups']            = 'websoul/Group_ctl';
$route['websoul/group/add']         = 'websoul/Group_ctl/add';
$route['websoul/group/edit/(:any)'] = 'websoul/Group_ctl/edit/$1';
$route['websoul/group/api/(:any)']  = 'websoul/Group_ctl/$1';

// Menus
$route['websoul/menus']            = 'websoul/Menu_ctl';
$route['websoul/menu/add']         = 'websoul/Menu_ctl/add';
$route['websoul/menu/edit/(:any)'] = 'websoul/Menu_ctl/edit/$1';
$route['websoul/menu/api/(:any)']  = 'websoul/Menu_ctl/$1';


/* End Websoul */



/* Site */
$route['home']         = 'Home_ctl';
$route['news']         = 'site/News_ctl';
$route['biography']    = 'site/Biography_ctl';
$route['constitution'] = 'site/Constitution_ctl';
$route['journal']      = 'site/Journal_ctl';
$route['profession']   = 'site/Profession_ctl';
$route['umkm']         = 'site/Umkm_ctl';
$route['info']         = 'site/Info_ctl';
$route['package']      = 'site/Package_ctl';
$route['sign-in']      = 'site/Account_ctl/login';
$route['sign-up']      = 'site/Account_ctl/register';

/* End Site */